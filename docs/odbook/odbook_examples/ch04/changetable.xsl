<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
    xmlns:meta="urn:oasis:names:tc:opendocument:xmlns:meta:1.0"
    xmlns:config="urn:oasis:names:tc:opendocument:xmlns:config:1.0"
    xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0"
    xmlns:table="urn:oasis:names:tc:opendocument:xmlns:table:1.0"
    xmlns:draw="urn:oasis:names:tc:opendocument:xmlns:drawing:1.0"
    xmlns:presentation="urn:oasis:names:tc:opendocument:xmlns:presentation:1.0"
    xmlns:dr3d="urn:oasis:names:tc:opendocument:xmlns:dr3d:1.0"
    xmlns:chart="urn:oasis:names:tc:opendocument:xmlns:chart:1.0"
    xmlns:form="urn:oasis:names:tc:opendocument:xmlns:form:1.0"
    xmlns:script="urn:oasis:names:tc:opendocument:xmlns:script:1.0"
    xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
    xmlns:number="urn:oasis:names:tc:opendocument:xmlns:datastyle:1.0"
    xmlns:anim="urn:oasis:names:tc:opendocument:xmlns:animation:1.0"

    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:math="http://www.w3.org/1998/Math/MathML"
    xmlns:xforms="http://www.w3.org/2002/xforms"

    xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0"
    xmlns:svg="urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0"
    xmlns:smil="urn:oasis:names:tc:opendocument:xmlns:smil-compatible:1.0"
>

<xsl:output method="xml" indent="yes"/>

<xsl:param name="sort"/>

<xsl:template match="/">
	<office:document-content
		xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
	    xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
	    xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0"
		xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0"
		xmlns:dc="http://purl.org/dc/elements/1.1/"
		xmlns:table="urn:oasis:names:tc:opendocument:xmlns:table:1.0">
    <office:scripts/>

    <office:font-face-decls>
        <style:font-face style:name="Bitstream Charter"
			svg:font-family="&apos;Bitstream Charter&apos;"
			style:font-pitch="variable"/>
    </office:font-face-decls>

    <office:automatic-styles>
        <style:style style:name="P1" style:family="paragraph">
            <style:text-properties style:font-name="Bitstream Charter"
            fo:font-size="10pt" style:font-size-asian="10pt"
            style:font-size-complex="10pt"/>
 		</style:style>
			
		<style:style style:name="P2" style:family="paragraph">
			<style:paragraph-properties fo:text-align="center"/>
 			<style:text-properties style:font-name="Bitstream Charter"
            fo:font-size="10pt" style:font-size-asian="10pt"
            style:font-size-complex="10pt"
			fo:text-align="center"
			fo:font-style="italic"
			fo:font-weight="bold"/>
 		</style:style>

		<style:style style:name="ctable" style:family="table">
			<style:table-properties
				style:width="15cm" table:align="center" />
		</style:style>
		
		<style:style style:name="ctable.A" style:family="table-column">
			<style:table-column-properties style:column-width="4.5cm" />
		</style:style>

		<style:style style:name="ctable.B" style:family="table-column">
			<style:table-column-properties style:column-width="7cm"/>
		</style:style>

		<style:style style:name="ctable.C" style:family="table-column">
			<style:table-column-properties style:column-width="3.5cm"/>
		</style:style>
		
		<style:style style:name="ctable.A1" style:family="table-cell">
			<style:table-cell-properties
				fo:border-top="0.035cm solid #000000"
				fo:border-right="none"
				fo:border-bottom="0.035cm solid #000000"
				fo:border-left="0.035cm solid #000000"
				fo:padding="0.10cm">
				<xsl:call-template name="set-bg-color">
					<xsl:with-param name="col-type">time</xsl:with-param>
				</xsl:call-template>
			</style:table-cell-properties>		
		</style:style>

		<style:style style:name="ctable.B1" style:family="table-cell">
			<style:table-cell-properties
				fo:border-top="0.035cm solid #000000"
				fo:border-right="none"
				fo:border-bottom="0.035cm solid #000000"
				fo:border-left="0.035cm solid #000000"
				fo:padding="0.10cm">
				<xsl:call-template name="set-bg-color">
					<xsl:with-param name="col-type">author</xsl:with-param>
				</xsl:call-template>
			</style:table-cell-properties>
		</style:style>
		
		<style:style style:name="ctable.C1" style:family="table-cell">
			<style:table-cell-properties
				fo:border="0.035cm solid #000000"
				fo:padding="0.10cm">
				<xsl:call-template name="set-bg-color">
					<xsl:with-param name="col-type">type</xsl:with-param>
				</xsl:call-template>
			</style:table-cell-properties>
		</style:style>

		<style:style style:name="ctable.A2" style:family="table-cell">
			<style:table-cell-properties
				fo:border-top="none"
				fo:border-right="none"
				fo:border-bottom="0.035cm solid #000000"
				fo:border-left="0.035cm solid #000000"
				fo:padding="0.10cm">			
				<xsl:call-template name="set-bg-color">
					<xsl:with-param name="col-type">time</xsl:with-param>
				</xsl:call-template>
			</style:table-cell-properties>			
		</style:style>

		<style:style style:name="ctable.B2" style:family="table-cell">
			<style:table-cell-properties
				fo:border-top="none"
				fo:border-right="none"
				fo:border-bottom="0.035cm solid #000000"
				fo:border-left="0.035cm solid #000000"
				fo:padding="0.10cm">
				<xsl:call-template name="set-bg-color">
					<xsl:with-param name="col-type">author</xsl:with-param>
				</xsl:call-template>
			</style:table-cell-properties>			
		</style:style>
		
		<style:style style:name="ctable.C2" style:family="table-cell">
			<style:table-cell-properties
				fo:border-top="none"
				fo:border-right="0.035cm solid #000000"
				fo:border-bottom="0.035cm solid #000000"
				fo:border-left="0.035cm solid #000000"
				fo:padding="0.10cm">			
				<xsl:call-template name="set-bg-color">
					<xsl:with-param name="col-type">type</xsl:with-param>
				</xsl:call-template>
			</style:table-cell-properties>			
		</style:style>

    </office:automatic-styles>

    <office:body>	
		<office:text>		
			<table:table table:name="ctable" table:style-name="ctable">
				<table:table-column table:style-name="ctable.A" />
				<table:table-column table:style-name="ctable.B" />
				<table:table-column table:style-name="ctable.C" />
				<table:table-header-rows>
					<table:table-row>
						<table:table-cell table:style-name="ctable.A1"
							table:value-type="string">
							<text:h text:style-name="P2">Time</text:h>
						</table:table-cell>
						<table:table-cell table:style-name="ctable.B1"
							table:value-type="string">
							<text:h text:style-name="P2">Author</text:h>
						</table:table-cell>
						<table:table-cell table:style-name="ctable.C1"
							table:value-type="string">
							<text:h text:style-name="P2">Type</text:h>
						</table:table-cell>
					</table:table-row>
				</table:table-header-rows>
			
				<xsl:choose>
					<xsl:when test="$sort = 'time' or $sort = 'author'">
						<xsl:apply-templates
							select="office:document-content/office:body/
							office:text/text:tracked-changes"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:apply-templates
							select="office:document-content/office:body/
							office:text/text:tracked-changes/
							text:changed-region[text:insertion]"/>
						<xsl:apply-templates
							select="office:document-content/office:body/
							office:text/text:tracked-changes/
							text:changed-region[text:deletion]"/>
						<xsl:apply-templates
							select="office:document-content/office:body/
							office:text/text:tracked-changes/
							text:changed-region[text:format-change]"/>
					</xsl:otherwise>
				</xsl:choose>
			</table:table>
		</office:text>
    </office:body>
</office:document-content>
</xsl:template>

<xsl:template match="text:tracked-changes">
	<xsl:choose>
	<xsl:when test="$sort = 'time'">
		<xsl:apply-templates select="text:changed-region">
			<xsl:sort
				select="descendant::office:change-info/dc:date"/>
		</xsl:apply-templates>
	</xsl:when>
	<xsl:when test="$sort = 'author'">
		<xsl:apply-templates select="text:changed-region">
			<xsl:sort
				select="descendant::office:change-info/dc:creator"/>
		</xsl:apply-templates>
	</xsl:when>
	</xsl:choose>
</xsl:template>

<xsl:template match="text:changed-region">
	<table:table-row>
		<table:table-cell table:style-name="ctable.A2">
			<text:p text:style-name="P1">
				<xsl:call-template name="format-time">
					<xsl:with-param name="time"
						select="descendant::office:change-info/dc:date"/>
				</xsl:call-template>
			</text:p>
		</table:table-cell>
	
		<table:table-cell table:style-name="ctable.B2">
			<text:p text:style-name="P1">
				<xsl:value-of
					select="descendant::office:change-info/dc:creator"/>
			</text:p>
		</table:table-cell>
	
		<table:table-cell table:style-name="ctable.C2">
			<text:p text:style-name="P1">
				<xsl:choose>
					<xsl:when test="text:insertion">
						<xsl:text>Insertion</xsl:text>
					</xsl:when>
					<xsl:when test="text:deletion">
						<xsl:text>Deletion</xsl:text>
					</xsl:when>
					<xsl:when test="text:format-change">
						<xsl:text>Format Change</xsl:text>
					</xsl:when>
				</xsl:choose>
			</text:p>
		</table:table-cell>
	</table:table-row>
</xsl:template>

<xsl:template name="set-bg-color">
	<xsl:param name="col-type"/>
	<xsl:if test="$sort = $col-type">
		<xsl:attribute name="fo:background-color">#ddffdd</xsl:attribute>
	</xsl:if>
</xsl:template>

<xsl:template name="format-time">
	<xsl:param name="time"/>
	<xsl:value-of select="substring-before($time, 'T')"/>
	<xsl:text> </xsl:text>
	<xsl:value-of select="substring(substring-after($time, 'T'),1,5)"/>
</xsl:template>

<xsl:template match="text()"/>
</xsl:stylesheet>
