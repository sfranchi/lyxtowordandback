#!/usr/bin/perl

use Archive::Zip;
use Archive::Zip::MemberRead;
use Carp;
use strict 'vars';

my $zip;		# the zip file
my $fh;			# filehandle to the member being read
my $buffer;		# 32 kilobyte buffer

#
#	Extract a single XML file from an OpenOffice.org file
#	Output goes to standard output
#
if (scalar @ARGV != 2)
{
	croak("Usage: $0 document xmlfilename");
}

$zip = new Archive::Zip($ARGV[0]);
if (!$zip)
{
	croak("$ARGV[0] cannot be opened as a .ZIP file");
}

$fh  = new Archive::Zip::MemberRead($zip, $ARGV[1]);
if (!$fh)
{
	croak("$ARGV[0] does not contain a file named $ARGV[1]");
}

while ($fh->read($buffer, 32*1024))
{
	print $buffer;
}
