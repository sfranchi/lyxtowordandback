#!/usr/bin/perl

#
#   Show meta-information in an OpenOffice.org document.
#
use XML::Simple;
use IO::File;
use Text::Wrap;
use Carp;
use strict;

my $suffix;     # file suffix

#
#   Check for one argument: the name of the OpenOffice.org document
#
if (scalar @ARGV != 1)
{
    croak("Usage: $0 document");
}

#
#   Get file suffix for later reference
#
($suffix) = $ARGV[0] =~ m/\.(\w\w\w)$/;

#
#   Parse and collect information into the $meta hash reference
#
$ARGV[0] =~ s/[;|'"]//g;  #eliminate dangerous shell metacharacters     
my $fh = IO::File->new("perl member_read.pl $ARGV[0] meta.xml |");
my $xml= XMLin( $fh, forcearray => [ 'meta:keyword'] );
my $meta= $xml->{'office:meta'};

#
#   Output phase
#
print "Title:       $meta->{'dc:title'}\n"
    if ($meta->{'dc:title'});
print "Subject:     $meta->{'dc:subject'}\n"
    if ($meta->{'dc:subject'});

if ($meta->{'dc:description'})
{
    print "Description:\n";
    $Text::Wrap::columns = 60;
    print wrap("\t", "\t", $meta->{'dc:description'}), "\n";
}

print "Created:     ";
print format_date($meta->{'meta:creation-date'});
print " by $meta->{'meta:initial-creator'}"
    if ($meta->{'meta:initial-creator'});
print "\n";

print "Last edit:   ";
print format_date($meta->{"dc:date"});
print " by $meta->{'dc:creator'}"
    if ($meta->{'dc:creator'});
print "\n";

# Display keywords (which all appear to be in a single element)
#
print "Keywords:    ", join( ' - ',
  @{$meta->{'meta:keywords'}->{'meta:keyword'}}), "\n"
    if( $meta->{'meta:keywords'});

#
#   Take attributes from the meta:document-statistic element
#   (if any) and put them into the $info hash
#
my $statistics= $meta->{'meta:document-statistic'};
if ($suffix eq "sxw")
{
		print "Pages:       $statistics->{'meta:page-count'}\n";
		print "Words:       $statistics->{'meta:word-count'}\n";
		print "Tables:      $statistics->{'meta:table-count'}\n";
		print "Images:      $statistics->{'meta:image-count'}\n";
}
elsif ($suffix eq "sxc")
{
		print "Sheets:      $statistics->{'meta:table-count'}\n";
		print "Cells:       $statistics->{'meta:cell-count'}\n"
				if ($statistics->{'meta:cell-count'});
}

#
#   A convenience subroutine to make dates look
#   prettier than ISO-8601 format.
#
sub format_date
{
    my $date = shift;
    my ($year, $month, $day, $hr, $min, $sec);
    my @monthlist = qw (Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec);
    
    ($year, $month, $day, $hr, $min, $sec) =
        $date =~ m/(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2})/;
    return "$hr:$min on $day $monthlist[$month-1] $year";
}   
