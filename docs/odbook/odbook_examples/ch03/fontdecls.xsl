<!-- save this in file fontdecls.xslt -->
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
    xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
    xmlns:svg="urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0"
    xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0">

<xsl:template name="insert-font-decls">
    <office:font-face-decls>
        <style:font-face style:name="Arial Unicode MS"
            svg:font-family="'Arial Unicode MS'"
        style:font-pitch="variable" />
		<style:font-face style:name="Bitstream Vera Sans"
			svg:font-family="&apos;Bitstream Vera Sans&apos;"
			style:font-family-generic="swiss"
			style:font-pitch="variable"/>
		<style:font-face style:name="Bitstream Vera Sans1"
			svg:font-family="&apos;Bitstream Vera Sans&apos;"
			style:font-pitch="variable"/>
		<style:font-face style:name="Bitstream Vera Serif"
			svg:font-family="&apos;Bitstream Vera Serif&apos;"
			style:font-family-generic="roman"
			style:font-pitch="variable"/>
         <style:font-face style:name="Lucidasans"
		 	svg:font-family="Lucidasans"
			style:font-pitch="variable"/>
		<style:font-face style:name="Palatino"
            svg:font-family="Palatino"
			style:font-family-generic="roman"
			style:font-pitch="variable" />
        <style:font-face style:name="Bitstream Vera Sans Mono"
			svg:font-family="&apos;Bitstream Vera Sans Mono&apos;"
			style:font-pitch="fixed"/>
    </office:font-face-decls>
</xsl:template>
</xsl:stylesheet>
