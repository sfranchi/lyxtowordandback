<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0">

<xsl:output method="text"/>
	
<xsl:include href="whitespace_templates.xsl"/>

<xsl:template match="document">
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="text:p">
	<xsl:text>|</xsl:text>
	<xsl:apply-templates/>
	<xsl:text>|</xsl:text>
</xsl:template>

</xsl:stylesheet>
