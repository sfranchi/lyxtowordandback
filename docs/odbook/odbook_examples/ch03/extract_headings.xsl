<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
    xmlns:script="urn:oasis:names:tc:opendocument:xmlns:script:1.0"
    xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
    xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0"
    xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0"
>

<xsl:include href="fontdecls.xsl"/>

<xsl:template match="/office:document-content">
	<office:document-content
	xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
    xmlns:script="urn:oasis:names:tc:opendocument:xmlns:script:1.0"
    xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
    xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0"
	>
    <office:scripts/>

	<office:automatic-styles>
		<style:style style:name="P1" style:family="paragraph">
            <style:paragraph-properties>
                <style:tab-stops>
                    <style:tab-stop style:position="1cm" />
                    <style:tab-stop style:position="2cm" />
                    <style:tab-stop style:position="3cm" />
                    <style:tab-stop style:position="4cm" />
                    <style:tab-stop style:position="5cm" />
                    <style:tab-stop style:position="6cm" />
                    <style:tab-stop style:position="7cm" />                  
                </style:tab-stops>
			</style:paragraph-properties>
			<style:text-properties
				 fo:font-size="10pt" style:font-size-asian="12pt"
				 style:font-size-complex="12pt"/>
		</style:style>
	</office:automatic-styles>
	<office:body>
		<office:text>
			<xsl:apply-templates select="//text:h"/>
		</office:text>
	</office:body>
	</office:document-content>
</xsl:template>

<xsl:template match="text:h">
	<text:p text:style-name="P1">
	<xsl:call-template name="emit-tabs">
		<xsl:with-param name="n" select="@text:outline-level - 1"/>
	</xsl:call-template>
	<xsl:value-of select="."/>
	</text:p>
</xsl:template>

<xsl:template name="emit-tabs">
	<xsl:param name="n" select="0"/>
	<xsl:if test="$n != 0">
		<text:tab/>
		<xsl:call-template name="emit-tabs">
			<xsl:with-param name="n" select="$n - 1"/>
		</xsl:call-template>
	</xsl:if>
</xsl:template>
</xsl:stylesheet>
