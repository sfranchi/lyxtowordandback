<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0"
	xmlns:java="ODWhiteSpace"
	exclude-result-prefixes="java">

<xsl:template match="/document">
<office:document-content
    xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
    xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
    xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0"
	office:version="1.0">
    <office:body>
		<office:text>
			<xsl:apply-templates select="test"/>
		</office:text>
	</office:body>
</office:document-content>
</xsl:template>

<xsl:template match="test">
<xsl:variable name="str" select="."/>
<text:p><xsl:copy-of select="java:compressString($str)"/></text:p>
</xsl:template>

</xsl:stylesheet>
