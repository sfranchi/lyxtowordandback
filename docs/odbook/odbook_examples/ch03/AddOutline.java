import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import java.util.Date;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;
import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentType;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import org.apache.xerces.parsers.DOMParser;
import org.apache.xml.serialize.XMLSerializer;
import org.apache.xml.serialize.OutputFormat;

import org.xml.sax.InputSource;

public class AddOutline
{
    /* The parsed document */
    protected Document document = null;
    
    /* Permanent pointer to root element of output document */
    protected Element documentRoot;

	/* List of heading elements */
	protected NodeList headingElements;

	/* Last numbers for paragraph, list, and section styles */
	int			lastParaNumber = 0;
	int			lastListNumber = 0;
	int			lastSectionNumber = 0;

	/* Node locations of last paragraph, list, and section styles */
	Node		lastParaNode = null;
	Node		lastListNode = null;
	Node		lastSectionNode = null;

	/* File descriptors */
	File		inFile;
	File		outFile;
	
	/* Streams for reading and writing jar files */
	JarInputStream	inJar;
	JarOutputStream	outJar;
		
    public static void main(String argv[]) {

        // check for proper number of arguments
        if (argv.length != 2) {
            System.err.println("usage: java AddOutline filename newfilename");
            System.exit(1);
        }
		
        AddOutline adder = null;

        adder = new AddOutline();
		adder.modifyDocument(argv);  

    } // main(String[])

	protected void modifyDocument(String argv[])
	{
		JarEntry inEntry;
		JarEntry outEntry;

		/* Create file descriptors */
		inFile = new File(argv[0]);
		outFile = new File(argv[1]);

		openInputFile();

		/* Get the manifest from the input file */
		Manifest manifest = inJar.getManifest( );
				
		/* Open output file, copying manifest if it existed */
		try
		{
			if (manifest == null)
			{
				outJar = new JarOutputStream(new FileOutputStream(outFile));
			}
			else
			{
				outJar = new JarOutputStream(new FileOutputStream(outFile),
					manifest);
			}
		}
		catch (IOException e)
		{
			System.err.println("Unable to open output file.");
			System.exit(1);
		}
		
		try
		{
			byte	buffer[] = new byte[16384];
			int		nRead;

			while ((inEntry = inJar.getNextJarEntry()) != null)
			{
//System.err.println(inEntry.getName());
				if (!inEntry.getName().equals("content.xml"))
				{
					/*
					 * Create output entry based on information in
					 * corresponding input entry
					 */
					outEntry = new JarEntry(inEntry);
					outJar.putNextEntry(outEntry);
					while ((nRead = inJar.read(buffer, 0, 16384)) != -1)
					{
						outJar.write(buffer, 0, nRead);
					}
				}
			}
			
			inJar.close();
			openInputFile();

			while ((inEntry = inJar.getNextJarEntry()) != null &&
				!(inEntry.getName().equals("content.xml")))
			{
				/* do nothing */
			}

			/*
			 * Create output entry based on information in
			 * corresponding input entry, but update its
			 * timestamp.
			 */
			outEntry = new JarEntry(inEntry);
			outEntry.setTime(new Date().getTime());
			outJar.putNextEntry(outEntry);
			document = readContent();	/* parse content.xml */
        	processContent();	/* add styles and bulleted list */
        	writeContent();		/* write it to output JAR file */

			outJar.close();
		}
		catch (IOException e)
		{
			System.err.println("Error while creating new file");
			e.printStackTrace();
		}
	}

	public void openInputFile()
	{
		try
		{
			inJar = new JarInputStream(new FileInputStream(inFile));
		}
		catch (IOException e)
		{
			System.err.println("Unable to open input file.");
			System.exit(1);
		}
	}

    public void processContent()
    {
		Node		autoStyles;
		Element		officeBodyStart;	/* the <office:body> element */
		Element		officeTextStart;	/* the <office:text> element */
		Element		textStart;			/* place to insert new text */
		Element		element;
		
		if (document == null)
		{
			return;
		}
		
		documentRoot = (Element) document.getDocumentElement();

		headingElements = document.getElementsByTagName("text:h");
		if (headingElements.getLength() == 0)
		{
			return;
		}
		
		autoStyles = findFirstChild(documentRoot, "office:automatic-styles");
		findLastItems(autoStyles);

		/*
		 * Prepare to add the new styles by going to the next
		 * available number. We'll insert the new style before
		 * the next sibling of the last node. We have to do this
		 * crazy thing because there is no "insertAfter" method.
		 */
		lastParaNumber++;
		lastListNumber++;
		lastSectionNumber++;
		if (lastParaNode != null)
		{
			lastParaNode = lastParaNode.getNextSibling();
		}
		if (lastListNode != null)
		{
			lastListNode = lastListNode.getNextSibling();
		}
		if (lastSectionNode != null)
		{
			lastSectionNode = lastSectionNode.getNextSibling();
		}
		
		/*
		 * Create a <style:style> element for the new paragraph,
		 * set its attributes and insert it after the last paragraph
		 * style.
		 */
		element = document.createElement("style:style");
		element.setAttribute("style:name", "P" + lastParaNumber);
		element.setAttribute("style:family", "paragraph");
		element.setAttribute("style:list-style-name", "L" + lastListNumber);
		element.setAttribute("style:parent-style-name", "Standard");		
		autoStyles.insertBefore(element, lastParaNode);

		/*
		 * Create a <style:style> element for the new section,
		 * set its attributes and insert it after the last section
		 * style.
		 */
		element = document.createElement("style:style");
		element.setAttribute("style:name", "Sect" + lastSectionNumber);
		element.setAttribute("style:family", "section");
		addSectionProperties(element);
		autoStyles.insertBefore(element, lastSectionNode);
	
		/*
		 * Create a <text:list-style> element for the new list,
		 * set its attributes and insert it after the last list
		 * style.
		 */
		element = document.createElement("text:list-style");
		element.setAttribute("style:name", "L" + lastParaNumber);
		addBullets(element);
		
		autoStyles.insertBefore(element, lastListNode);
		
		/*
		 * Now proceed to where we will add text;
		 * it's just after the first <text:sequence-decls>
		 * in the <office:text> in the <office:body> 
		 */
		officeBodyStart = findFirstChild(documentRoot, "office:body");
		officeTextStart = findFirstChild(officeBodyStart, "office:text");
		textStart = findFirstChild(officeTextStart, "text:sequence-decls");
		textStart = getNextElementSibling( textStart );
		
		/*
		 * Add a section
		 */
		element = document.createElement("text:section");
		element.setAttribute("text:style-name", "Sect" + lastSectionNumber);
		element.setAttribute("text:name", "Section" + lastSectionNumber);
		addHeadings(element);
		officeTextStart.insertBefore(element, textStart);
	}

	/* Find the last list, section, and paragraph styles */
	public void findLastItems(Node autoStyle)
	{
		NodeList	children;
		Node		child;
		Element		element;
		
		children = autoStyle.getChildNodes();

		for (int i = 0; i < children.getLength(); i++)
		{
			child = children.item(i);
			if (child.getNodeType() == Node.ELEMENT_NODE)
			{
				element = (Element) child;
				if (element.getTagName().equals("style:style"))
				{
					String styleName = element.getAttribute("style:name");
					if  (element.getAttribute("style:family")
						.equals("paragraph"))
					{
					int paraNumber =
						Integer.parseInt(styleName.substring(1));
					lastParaNumber = Math.max(paraNumber, lastParaNumber);
					lastParaNode = child;
					}
					else if (element.getAttribute("style:family")
						.equals("section"))
					{
						int sectionNumber =
							Integer.parseInt(styleName.substring(4));
						lastSectionNumber = Math.max(sectionNumber,
							lastSectionNumber);
						lastSectionNode = child;
					}
				}
				else if (element.getTagName().equals("text:list-style"))
				{
					String styleName = element.getAttribute("style:name");
					int listNumber =
						Integer.parseInt(styleName.substring(1));
					lastListNumber = Math.max(listNumber, lastListNumber);
					lastListNode = child;
				}
			}
		}
	}

	/*
	 * Find first element with a given tag name
	 * among the children of the given node.
	 */
	public Element findFirstChild(Node startNode, String tagName)
	{
//System.err.println("Starting findFirstChild at " + startNode);
		startNode = startNode.getFirstChild();
//System.err.println("StartNode first child is " + startNode);
		while (! (startNode != null &&
			startNode.getNodeType() == Node.ELEMENT_NODE &&
			((Element)startNode).getTagName().equals(tagName)))
		{
			startNode = startNode.getNextSibling();
//System.err.println("Next sib is " + startNode);
		}
		return (Element) startNode;
	}

	/*
	 *	Find next sibling that is an element
	 */
	public Element getNextElementSibling( Node node )
	{
		node = node.getNextSibling();
		while (node != null &&
			node.getNodeType() != Node.ELEMENT_NODE)
		{
			node = node.getNextSibling();
		}
		return (Element) node;
	}

	/*
	 * Add the appropriate properties to make a single-column
	 * section
	 */
	public void addSectionProperties(Element sectionStyle)
	{
		Element	properties;
		Element columns;
		
		properties = document.createElement("style:section-properties");
		properties.setAttribute("text:dont-balance-text-columns",
			"false");
		
		columns = document.createElement("style:columns");
		columns.setAttribute("fo:column-count", "0");
		columns.setAttribute("fo:column-gap", "0cm");
		
		properties.appendChild(columns);
		sectionStyle.appendChild(properties);
	}
	
	/*
	 * Add the ten bullet styles to the <text:list-style> element. 
	 */
	public void addBullets(Element listLevelStyle)
	{
		int		level;
		Element	bullet;
		Element	properties;
		for (level = 1; level <= 10; level++)
		{
			bullet = document.createElement("text:list-level-style-bullet");
			bullet.setAttribute("text:level", Integer.toString(level));
			bullet.setAttribute("text:bullet-char", "\u2022");
			
			properties = document.createElement("style:list-level-properties");
			if (level != 1)
			{
				properties.setAttribute("text:space-before",
					Double.toString((level-1) * 0.5) + "cm");
			}
			properties.setAttribute("text:min-label-width", "0.5cm");
			bullet.appendChild(properties);
			listLevelStyle.appendChild(bullet);
		}
	}
	
	/* Add headings to a section */
	public void addHeadings(Element startElement)
	{
		int	currentLevel = 0;
		int	headingLevel;
		int	i;
		int level;
		Element	ulist[] = new Element[10];
		Element listItem;
		Element paragraph;
		Text	textNode;
		
		for (i=0; i < headingElements.getLength(); i++)
		{
			headingLevel = Integer.parseInt(
				((Element)headingElements.item(i)).getAttribute("text:outline-level"));
//System.err.println("Current level is " + currentLevel + "; heading level is " + headingLevel);			
			if (headingLevel > currentLevel)
			{
				for (level = currentLevel; level < headingLevel; level++)
				{
					ulist[level] = document.createElement(
						"text:list");
					if (level == 0)
					{
						ulist[level].setAttribute("text:style-name",
							"L" + lastListNumber);
					}
				}
				currentLevel = headingLevel;
			}
			else if (headingLevel < currentLevel)
			{
				closeLists(ulist, currentLevel, headingLevel);
				currentLevel = headingLevel;
			}
			
			/* Now append this heading as an item to current level */

			listItem = document.createElement("text:list-item");
			paragraph = document.createElement("text:p");
			paragraph.setAttribute( "text:style-name", "P" + lastParaNumber );
			textNode = document.createTextNode("");
			textNode = accumulateText(
				(Element) headingElements.item(i),
				textNode);
			paragraph.appendChild(textNode);
			listItem.appendChild(paragraph);
			ulist[currentLevel-1].appendChild(listItem);

		}
		if (currentLevel != 1)
		{
			closeLists(ulist, currentLevel, 1);
		}
		startElement.appendChild(ulist[0]);
	}	

	/*
	 * Join elements in the ulist[] array to close all open lists
	 * from currentLevel back down to newLevel
	 */
	public void closeLists(Element ulist[], int currentLevel, int newLevel)
	{
		int i;
		Element listItem;
//System.err.println("Popping levels " + currentLevel + " to " + (newLevel));
		for (i = currentLevel-1; i > newLevel-1; i--)
		{
			listItem = document.createElement("text:list-item");
			listItem.appendChild(ulist[i]);
//System.err.println("Joining to level " + (i-1));
			ulist[i-1].appendChild(listItem);
		}
	}

	/*
 	* Return a Text node that contains all the accumulated text
 	* from the child nodes of the given element.
 	*/
	public Text accumulateText(Element element, Text text)
	{
		Node node = element.getFirstChild();
		while (node != null)
		{
			if (node.getNodeType() == Node.TEXT_NODE)
			{
				text.appendData(((Text) node).getData());
			}
			else if (node.getNodeType() == Node.ELEMENT_NODE)
			{
				if (((Element) node).getTagName().equals("text:tab"))
				{
					text.appendData(" ");
				}
				else
				{
					text = accumulateText((Element) node, text);
				}
			}
			node = node.getNextSibling();
		}
		return text;
	}
	
    /* Read the input document and construct a document tree. */
    public Document readContent( )
	{
        try
		{
            DOMParser parser = new org.apache.xerces.parsers.DOMParser();
			parser.setEntityResolver(new ResolveDTD());
            parser.parse(new InputSource(inJar));
            return parser.getDocument();
        }
		catch (Exception e)
		{
            e.printStackTrace(System.err);
			return null;
        }
    }

	
    public void writeContent()
	{
        if (document == null)
        {
            return;
        }
        PrintWriter out = null;
        try
		{
            out =
            new PrintWriter(new OutputStreamWriter(outJar, "UTF-8"));
        }
        catch (Exception e)
        {
            System.out.println("Error creating output stream");
            System.out.println(e.getMessage());
            System.exit(1);
        }
        OutputFormat oFormat = new OutputFormat("xml", "UTF-8", false);
        XMLSerializer serial = new XMLSerializer(out, oFormat);
        try
        {
            serial.serialize(document);
        }
        catch (java.io.IOException e)
        {
            System.out.println(e.getMessage());
        }
    }
}

