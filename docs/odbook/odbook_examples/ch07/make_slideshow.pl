#!/usr/bin/perl
use strict;
use warnings;

use Archive::Zip;
use File::Spec;

my	$file_name;			# file name to insert in slide show
my	$description;		# description of the slide
my	($width, $height);	# height and width of a picture
my	$left_edge;			# where left edge of picture begins

my 	$archive;			# zip archive
my	$content_filename;	# name of content file
my	$styles_filename;	# name of styles file
my	$manifest_filename;	# name of the manifest file
my	@piclist; 

my	$i;					# ubiquitous loop counter
my	$slide_number;		# current slide number

#
#	This program requires two arguments: the picture list
#	and the output file name.

if (scalar @ARGV != 2)
{
	print "Usage: $0 picturelist outputFilename\n";
	exit;
}

#	Read the picture file names and their descriptions
#	into an array
#
open INFILE, "<$ARGV[0]" or die("Cannot find file $ARGV[0]");
while (<INFILE>)
{
	chomp;
	next if (m/^\s*$/);	# skip blank lines
	push @piclist, $_;
}
close INFILE;

#
#	Create a .ZIP archive
#
$archive = Archive::Zip->new( );

#
#	Add a directory for the images
#
$archive->addDirectory( "Pictures" );

#
#	create a temporary file for the manifest,
#	create the META-INF/manifest.xml file, and
#	add it to the archive
#
$manifest_filename = File::Spec->catfile(File::Spec->tmpdir(),
	"manifest.xml");
open MANIFESTFILE, ">$manifest_filename" or
	die("Cannot open $manifest_filename");

create_manifest_file( );

close MANIFESTFILE;
$archive->addFile( $manifest_filename , "META-INF/manifest.xml" );

#	create a temporary file for styles,
#	create the styles.xml file, and
#	add it to the archive.
#
$styles_filename = File::Spec->catfile(File::Spec->tmpdir(), "styles.xml");
open STYLESFILE, ">$styles_filename" or
	die("Cannot open $styles_filename");

create_styles_file( );

close STYLESFILE;

$archive->addFile( $styles_filename , "styles.xml" );

#	create a temporary file for content
#
$content_filename = File::Spec->catfile(File::Spec->tmpdir(), "content.xml");
open CONTENTFILE, ">$content_filename" or
	die("Cannot open $content_filename");


insert_header();

for ($i=0; $i < @piclist; $i += 2)
{
	$slide_number = ($i / 2) + 1;
	($file_name, $description) = @piclist[$i,$i+1];
	
	print STDERR "Processing $file_name ($description)\n";
	
	($width, $height) = get_jpg_dimensions($file_name);
	
	# Presume that pictures are 72 dpi; convert width & height
	# to centimeters
	#
	$width = ($width / 72) * 2.54;
	$height = ($height / 72) * 2.54;
	$left_edge = (27.94-$width) / 2;
	
	$archive->addFile( $file_name, "Pictures/$file_name");
	print CONTENTFILE << "ONE_PAGE";
<draw:page draw:name="page$slide_number"
 draw:master-page-name="PM1" draw:style-name="dp7"
 presentation:presentation-page-layout-name="slidepage">
	<draw:frame draw:layer="layout" presentation:class="title"
		presentation:style-name="standard"
		svg:x="4.5cm" svg:y="2.25cm"
        svg:width="20cm" svg:height="2cm">
		<draw:text-box>
			<text:p text:style-name="P1">$description</text:p>
		</draw:text-box>
	</draw:frame>
	<draw:frame draw:layer="layout"
	  svg:width="${width}cm" svg:height="${height}cm"
	  svg:x="${left_edge}cm" svg:y="4cm">
	  	<draw:image
			xlink:href="Pictures/$file_name"
			xlink:type="simple" xlink:show="embed"
			xlink:actuate="onLoad"/>
	</draw:frame>
ONE_PAGE
	if ($slide_number != 1)
	{
		insert_back_button();
	}
	if ($slide_number != int(@piclist/2))
	{
		insert_next_button();
	}
	print CONTENTFILE "</draw:page>\n";

}

insert_footer();

close CONTENTFILE;

$archive->addFile( $content_filename , "content.xml" );
$archive->overwriteAs( $ARGV[1] );

unlink $styles_filename;
unlink $content_filename;
unlink $manifest_filename;

sub insert_header
{
	print CONTENTFILE <<"HEADER";
<?xml version="1.0" encoding="UTF-8"?>
<office:document-content
	xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
	xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
	xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0"
	xmlns:table="urn:oasis:names:tc:opendocument:xmlns:table:1.0"
	xmlns:draw="urn:oasis:names:tc:opendocument:xmlns:drawing:1.0"
	xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0"
	xmlns:xlink="http://www.w3.org/1999/xlink"
	xmlns:number="urn:oasis:names:tc:opendocument:xmlns:datastyle:1.0"
	xmlns:presentation="urn:oasis:names:tc:opendocument:xmlns:presentation:1.0"
	xmlns:svg="urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0" 
	xmlns:form="urn:oasis:names:tc:opendocument:xmlns:form:1.0"
	xmlns:script="urn:oasis:names:tc:opendocument:xmlns:script:1.0" 
	xmlns:dom="http://www.w3.org/2001/xml-events"
	office:version="1.0">
 <office:scripts/>
 <office:automatic-styles>
	<style:style style:name="dp1" style:family="drawing-page">
		<style:properties
		 presentation:transition-speed="medium"
		 presentation:background-visible="true"
		 presentation:background-objects-visible="true"
		 smil:type="barWipe"
		 smil:subtype="leftToRight"/>
	</style:style>
	<style:style style:name="P1" style:family="paragraph">
		<style:paragraph-properties fo:text-align="center"/>
		<style:text-properties fo:font-size="18pt"/>
	</style:style>
	<style:style style:name="buttonborderstyle" style:family="graphic">
		<style:graphic-properties
			draw:stroke="solid"
        	svg:stroke-width="0.05cm"
        	svg:stroke-color="#cccccc"
        	draw:fill="solid" draw:fill-color="#ffffcc"/>
	</style:style>
	<style:style style:name="buttontext" style:family="text">
		<style:text-properties
			fo:font-family="Helvetica"
			style:font-family-generic="swiss"
			fo:font-size="10pt"/>
	</style:style>
 </office:automatic-styles>
 <office:body>
 	<office:presentation>
HEADER
}

sub insert_footer
{
	print CONTENTFILE <<"FOOTER";
	</office:presentation>
 </office:body>
</office:document-content>
FOOTER
}

sub insert_back_button
{
	print CONTENTFILE << "BACK_BUTTON";
<draw:rect draw:layer="layout" draw:text-style-name="centered"
 svg:x="2.2cm" svg:y="18.3cm"
 svg:width="2cm" svg:height="1cm"
 draw:style-name="buttonborderstyle">
    <office:event-listeners>
		<presentation:event-listener script:event-name="dom:click"
		 presentation:action="previous-page"/>
    </office:event-listeners>	
	<text:p text:style-name="P1">
		<text:span text:style-name="buttontext">Back</text:span>
	</text:p>
</draw:rect>
BACK_BUTTON
}

sub insert_next_button
{
	print CONTENTFILE << "NEXT_BUTTON";
<draw:rect draw:layer="layout"
 svg:x="24cm" svg:y="18.3cm"
 svg:width="2cm" svg:height="1cm"
 draw:style-name="buttonborderstyle">
    <office:event-listeners>
		<presentation:event-listener script:event-name="dom:click"
		 presentation:action="next-page"/>
    </office:event-listeners>	
	<text:p text:style-name="P1">
		<text:span text:style-name="buttontext">Next</text:span>
	</text:p>
</draw:rect>
NEXT_BUTTON
}


sub create_styles_file
{
	print STYLESFILE << "STYLES";
<office:document-styles
 xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
 xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
 xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0"
 xmlns:table="urn:oasis:names:tc:opendocument:xmlns:table:1.0"
 xmlns:draw="urn:oasis:names:tc:opendocument:xmlns:drawing:1.0"
 xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0"
 xmlns:xlink="http://www.w3.org/1999/xlink"
 xmlns:number="urn:oasis:names:tc:opendocument:xmlns:datastyle:1.0"
 xmlns:presentation="urn:oasis:names:tc:opendocument:xmlns:presentation:1.0"
 xmlns:svg="urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0"
 xmlns:dom="http://www.w3.org/2001/xml-events"
 office:version="1.0">

<office:styles>
    <style:presentation-page-layout style:name="slidepage">
        <presentation:placeholder presentation:object="title"
		  svg:x="4.5cm" svg:y="2.25cm"
		  svg:width="20cm" svg:height="2cm"/>
        <presentation:placeholder presentation:object="graphic"
          svg:x="2cm" svg:y="5.5cm"
          svg:width="20cm" svg:height="13cm"/>
    </style:presentation-page-layout>
</office:styles>

<office:automatic-styles>
	<style:page-master style:name="PM1">
		<style:properties
		 fo:margin-top="1cm" fo:margin-bottom="1cm"
		 fo:margin-left="1cm" fo:margin-right="1cm"
		 fo:page-width="27.94cm" fo:page-height="21.59cm"
		 style:print-orientation="landscape"/>
	</style:page-master>
</office:automatic-styles>

<office:master-styles>
    <draw:layer-set>
        <draw:layer draw:name="layout"/>
        <draw:layer draw:name="background"/>
        <draw:layer draw:name="backgroundobjects"/>
        <draw:layer draw:name="controls"/>
        <draw:layer draw:name="measurelines"/>
    </draw:layer-set>

    <style:master-page style:name="Default" style:page-master-name="PM1"
     draw:style-name="dp1">
    </style:master-page>
</office:master-styles>

</office:document-styles>
STYLES

}

sub create_manifest_file
{
	print MANIFESTFILE << "MANIFEST";
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE manifest:manifest
	PUBLIC "-//OpenOffice.org//DTD Manifest 1.0//EN" "Manifest.dtd">
<manifest:manifest
	xmlns:manifest="urn:oasis:names:tc:opendocument:xmlns:manifest:1.0">
	<manifest:file-entry
		manifest:media-type="application/vnd.oasis.opendocument.presentation"
		manifest:full-path="/"/>
	<manifest:file-entry manifest:media-type="text/xml"
		manifest:full-path="content.xml"/>
	<manifest:file-entry manifest:media-type="text/xml"
		manifest:full-path="styles.xml"/>
	<manifest:file-entry manifest:media-type="text/xml"
		manifest:full-path="meta.xml"/>
	<manifest:file-entry manifest:media-type=""
		manifest:full-path="Pictures/"/>
MANIFEST
	for ($i=0; $i < @piclist; $i += 2)
	{
		print MANIFESTFILE
			qq!<manifest:file-entry manifest:media-type="image/jpeg"!,
			qq! manifest:full-path="Pictures/$piclist[$i]"/>\n!;
	}
	print MANIFESTFILE "</manifest:manifest>\n";
}

sub get_jpg_dimensions
{
	my ($file_name) = @_;

    my ($width, $height);
	my ($n_read, $ff_byte, $tag_byte, $buf, $result);
	
	my ($l1, $l2, $len);

	$width = $height = 0;

	open FILE, $file_name;
	binmode FILE;
	
	# skip back to third byte...
	seek( FILE, 2, 0 );
	while (1)
	{
		#
		#	read in four bytes (0xff, tag byte, and 2-byte length)
		#	when the tag byte equals 0xc0, that is the part of the
		#	file which holds the dimensions
		#
		$n_read = read( FILE, $buf, 4);
		last if ($n_read != 4);

		($ff_byte, $tag_byte, $l1, $l2) = unpack("CCCC", $buf);
		last if ($ff_byte != 0xff);

		$len = $l1 * 256 + $l2;

		if ($tag_byte == 0xc0)
		{
			$n_read = read( FILE, $buf, 5 );
			last if ($n_read != 5);

			($tag_byte, $height, $width) = unpack("Cnn", $buf);
			last;
		}
		
		#
		#	if it's not the 0xc0 tag, then skip forwards to next area.
		#	we subtract 2 from the length since it includes the length
		#	bytes, which we've already read.
		#
		$result = seek( FILE, $len-2, 1);
		last if ($result == 0);
	}
	close FILE;
	return ($width, $height);
}			
