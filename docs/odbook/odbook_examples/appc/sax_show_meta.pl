#!/usr/bin/perl

#
#	Show meta-information in an OpenOffice.org document.
#
use XML::SAX;
use IO::File;
use Text::Wrap;
use Carp;
use strict 'vars';

my $suffix;		# file suffix

my $parser;		# instance of XML::SAX parser
my $handler;	# module that handles elements, etc.
my $filehandle;	# file handle for piped input

my $info;		# the hash returned from the parser
my @attributes;	# attributes from a returned element
my %attr_hash;	# hash of attribute names and values
#
#	Check for one argument: the name of the OpenOffice.org document
#
if (scalar @ARGV != 1)
{
	croak("Usage: $0 document");
}

#
#	Get file suffix for later reference
#
($suffix) = $ARGV[0] =~ m/\.(\w\w\w)$/;

#
#	Create an object containing handlers for relevant events.
#
$handler = MetaElementHandler->new();


#
#	Create a parser and tell it where to find the handlers.
#
$parser =
	XML::SAX::ParserFactory->parser( Handler => $handler);

#
#	Input to the parser comes from the output of member_read.pl
#		
$ARGV[0] =~ s/[;|'"]//g;  # eliminate dangerous shell metacharacters
$filehandle = IO::File->new( "perl member_read.pl $ARGV[0] meta.xml |" );

#
#	Parse and collect information.
#
$parser->parse_file( $filehandle );

#
#	Retrieve the information collected by the parser
#
$info = $handler->get_info();

#
#	Output phase
#
print "Title:       $info->{'dc:title'}[0]\n"
	if ($info->{'dc:title'}[0]);
print "Subject:     $info->{'dc:subject'}[0]\n"
	if ($info->{'dc:subject'}[0]);

if ($info->{'dc:description'}[0])
{
	print "Description:\n";
	$Text::Wrap::columns = 60;
	print wrap("\t", "\t", $info->{'dc:description'}[0]), "\n";
}

print "Created:     ";
print format_date($info->{'meta:creation-date'}[0]);
print " by $info->{'meta:initial-creator'}[0]"
	if ($info->{'meta:initial-creator'}[0]);
print "\n";

print "Last edit:   ";
print format_date($info->{"dc:date"}[0]);
print " by $info->{'dc:creator'}[0]"
	if ($info->{'dc:creator'}[0]);
print "\n";

# Display keywords (which all appear to be in a single element)
#
print "Keywords:    ", join( ' - ', $info->{'meta:keywords'}[0]), "\n"
    if( $info->{'meta:keywords'});

#
#	Take attributes from the meta:document-statistic element
#	(if any) and put them into %attr_hash.
#
@attributes = @{$info->{'meta:document-statistic'}};

if (scalar(@attributes) > 1)
{
	shift @attributes;
	%attr_hash = @attributes;

	if ($suffix eq "sxw")
	{
		print "Pages:       $attr_hash{'meta:page-count'}\n";
		print "Words:       $attr_hash{'meta:word-count'}\n";
		print "Tables:      $attr_hash{'meta:table-count'}\n";
		print "Images:      $attr_hash{'meta:image-count'}\n";
	}
	elsif ($suffix eq "sxc")
	{
		print "Sheets:      $attr_hash{'meta:table-count'}\n";
		print "Cells:       $attr_hash{'meta:cell-count'}\n"
			if ($attr_hash{'meta:cell-count'});
	}
}


#
#	A convenience subroutine to make dates look
#	prettier than ISO-8601 format.
#
sub format_date
{
	my $date = shift;
	my ($year, $month, $day, $hr, $min, $sec);
	my @monthlist = qw (Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec);
	
	($year, $month, $day, $hr, $min, $sec) =
		$date =~ m/(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2})/;
	return "$hr:$min on $day $monthlist[$month-1] $year";
}

########################################################

package MetaElementHandler;

my %element_info;	# the data structure that we are creating
my $element;		# name of element being processed
my @attributes;		# attributes for this element
my $text;			# text content of the element


sub new {
    my $class = shift;
    my %opts = @_;
    bless \%opts, $class;
}

sub reset {
    my $self = shift;
    %$self = ();
}

#
#	Store current element and its attribute.
#
sub start_element
{
    my ($self, $parser_data) = @_;
	
	my $hashref;
	my $item;		# loop control variable

	$element = $parser_data->{"Name"};

	foreach $item (keys %{$parser_data->{"Attributes"}})
	{
		$hashref =  $parser_data->{"Attributes"}{$item};
		push @attributes, $hashref->{"Name"},  $hashref->{"Value"};
	}
	
	$text = "";	# no text content yet.
}

#
#	Create an entry into a hash for the element that is ending
#
sub end_element
{
    my ($self, $parser_data) = @_;

	$element = $parser_data->{"Name"};
	$element_info{$element} = [$text, @attributes];
}

#
#	Accumulate element's text content.
#
sub characters
{
    my ($self, $parser_data) = @_;
	$text .= $parser_data->{"Data"};
}

#	Return a reference to the %info hash 
#
sub get_info
{
	my $self = shift;
	return \%element_info;
}
