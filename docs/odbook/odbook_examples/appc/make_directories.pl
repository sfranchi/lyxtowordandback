#
#   Command line parameter: pathname of directory to create
#
#   Creates directory and all intervening levels.
#
use File::Path;

if (scalar @ARGV == 1)
{
	mkpath($ARGV[0], 1, 0755);
}
else
{
	print "Usage: $0 path_to_create\n";
}

