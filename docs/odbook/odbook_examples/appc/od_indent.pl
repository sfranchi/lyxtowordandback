#!/usr/bin/perl

use File::Find;

#
#   This program indents XML files within a directory.
#   a simple XSLT transform is used to indent the XML.
#

#
#   Path where you have installed the OpenDocument transform script.
#
$script_location = "/your/path/to/odtransform.sh";

#
#	Path where you have installed the XSLT transformation.
#
$transform_location = "/your/path/to/od_indent.xsl";

if (scalar @ARGV != 1)
{
    print "Usage: $0 directory\n";
    exit;
}

if (!-e $script_location)
{
	print "Cannot find the transform script at $script_location\n";
	exit;
}

if (!-e $transform_location)
{
	print "Cannot find the XSLT transformation file at " ,
		"$transform_location\n";
	exit;
}	

$dir_name = $ARGV[0];

if (!-d $dir_name)
{
    print "The argument to $0 must be the name of a directory\n";
    print "containing XML files to be indented.\n";
    exit;
}

#
#   Indent all XML files.
#
find(\&indent, $dir_name);

#   Warning:
#   This subroutine creates a temporary file with the format
#   __tempnnnn.xml, where nnnn is the current time( ). This
#   will avoid name conflicts when used with OpenOffice.org documents,
#   even though the technique is not sufficiently robust for general use.
#
sub indent
{
    my $xmlfile = $_;
    my $command;
	my $result;
    if ($xmlfile =~ m/\.xml$/)
    {
        $time = time();
        print "Indenting $xmlfile\n";
        $command = "$script_location " .
            "-in $xmlfile -xsl $transform_location -out __temp$time.xml";
        $result = system( $command );
		if ($result == 0 && -e "__temp$time.xml")
		{
        	unlink $xmlfile;
        	rename "__temp$time.xml", $xmlfile;
		}	
		else
		{
			print "Error occurred while indenting $xmlfile\n";
		}	
    }
}
