import java.util.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.apache.xpath.NodeSet;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class ODWhiteSpace {

	public static NodeList compressString( String str )
	{
		ODWhiteSpace whiteSpace = new ODWhiteSpace();
// System.out.println("about to compress |" + str + "|");
		return whiteSpace.doCompress( str );
	}

	private Document tempDoc;		// necessary for creating elements
	private StringBuffer strBuf;	// where non-whitespace accumulates
	private NodeSet resultSet;		// the value to be returned
	private int pos;				// current position in string
	private int startPos;			// where blanks begin accumulating
	private int nSpaces;			// number of consecutive spaces
	private boolean inSpaces;		// handling spaces?
	private char ch;				// current character in buffer
	private char prevChar;			// previous character in buffer
	private Element element;		// element to be added to node list
	private final String textURI=
		"urn:oasis:names:tc:opendocument:xmlns:text:1.0";

	public NodeList doCompress( String str )
	{  
		if (str.length() == 0)
		{
			return null;
		}

		tempDoc = null;
		strBuf = new StringBuffer( str.length() );

	
		try
    	{
			tempDoc = DocumentBuilderFactory.newInstance().
				newDocumentBuilder().newDocument();
    	}
    	catch(ParserConfigurationException pce)
    	{
      		return null;
		}
 
		resultSet = new NodeSet();
		resultSet.setShouldCacheNodes(true);
		
		pos = 0;
		startPos = 0;
		nSpaces = 0;
		inSpaces = false;
		ch = '\u0000';

		while (pos < str.length())
		{
			prevChar = ch;
			ch = str.charAt( pos );
// System.out.println(pos + " |" + ch + "|");
			if (ch == ' ')
			{
				if (inSpaces)
				{
					nSpaces++;
// System.out.println("Added space: now " + nSpaces );
				}
				else
				{
// System.out.println("Starting spaces at " + pos);
					emitText( );
					nSpaces = 1;
					inSpaces = true;
					startPos = pos;
				}
			}
			else if (ch == 0x000a || ch == 0x000d)
			{
// System.out.println("Encountered LF or CR at " + pos );
				if (prevChar != 0x000d) // ignore LF or CR after CR.
				{
					emitPending( );
			   		element = tempDoc.createElement("text:line-break");
					resultSet.addNode(element);
				}      
			}
			else if (ch == 0x09)
			{
				emitPending( );
// System.out.println("Encountered TAB at " + pos );
			   	element = tempDoc.createElement("text:tab");
				resultSet.addNode(element);
			}
			else
			{
				if (inSpaces){ emitSpaces( ); }
// System.out.println("Appending |" + ch + "|");
				strBuf.append( ch );
			}
			pos++;
		}
		
		emitPending( );		// empty out anything that's accumulated
		
		return resultSet;
	}
	
	/**
	 * Emit accumulated spaces or text
	 */
	private void emitPending( )
	{
		if (inSpaces)
		{
			emitSpaces( );
		}
		else
		{
			emitText( );
		}
	}

	/*
	 * Emit accumulated text.
	 * Creates a text node with currently accumulated text.
	 * Side effect: emptiess accumulated text buffer
	 */
	private void emitText( )
	{
		if (strBuf.length() != 0)
		{
// System.out.println("Emit text |" + strBuf.toString( ) + "|");
			Text textNode = tempDoc.createTextNode( strBuf.toString( ) );
			resultSet.addNode( textNode );
			strBuf = new StringBuffer( );
		}
	}
	
	/*
	 * Emit accumulated spaces.
	 * If these are leading blanks, emit only a
	 * &lt;text:s&gt; element; otherwise a blank plus
	 * a &lt;text:s&gt; element (if necessary)
	 * Side effect: sets accumulated number of spaces to zero.
	 * Side effect: sets "inSpaces" flag to false
	 */
	private void emitSpaces( )
	{
		if (nSpaces != 0)
		{
// System.out.println("Emitting " + nSpaces + " spaces");
			if (startPos != 0)
			{
				Text textNode = tempDoc.createTextNode( " " );
				resultSet.addNode( textNode );
				nSpaces--;
			}

			if (nSpaces >= 1 || startPos == 0)
			{
				element = tempDoc.createElement( "text:s" );
				element.setAttribute( "text:c", 
					(new Integer(nSpaces)).toString( ) );
				resultSet.addNode( element );
			}

			inSpaces = false;
			nSpaces = 0;
		}
	}
}
