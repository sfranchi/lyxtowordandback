#!/usr/bin/perl

#
#	Repack a directory to an OpenDocument file
#
#	Directory xyz_odt will be packed into xyz.odt, etc.
#
#

use Archive::Zip;	# to zip files
use Cwd;			# to get current working directory

use strict;

my $dir_name;		# directory name to zip
my $file_name = "";	# destination file name
my $suffix;			# file extension
my $current_dir;	# current directory
my $zip;			# a zip file object

if (scalar @ARGV < 1 || scalar @ARGV > 2)
{
	print "Usage: $0 directoryname [newfilename]\n";
	exit;
}

$dir_name = $ARGV[0];

#
#	If no new filename is given, create a filename
#	based on directory name
#

if ($ARGV[1])
{
	$file_name = $ARGV[1];
}
else
{
	if ($dir_name =~ m/_(o[dt][tgpscif]|odm|oth)/)
	{
		$suffix = $1;
		($file_name = $dir_name) =~ s/(_$suffix)//;
		$file_name .= ".$suffix";
	}
	else
	{
		print "This does not appear to be an unpacked OpenDocument file.\n";
		print "Legal suffixes are _odt, _ott, _odg, _otg, _odp, _otp, _ods,\n";
		print "_ots, _odc, _otc, _odi, _oti, _odf, _otf, _odm, and _oth\n";
		$file_name = "";
	}
}

if ($file_name ne "")
{
	$zip = Archive::Zip->new();
	
	$current_dir = cwd();

	if (chdir($dir_name))
	{
		$zip->addTree( '.' );
		$zip->writeToFileNamed( "../$file_name" );
		print "$dir_name packed to $file_name.\n";
		chdir($current_dir);
	}
	else
	{
		print "Could not change directory to $dir_name\n";
	}
}

		
