#!/usr/bin/perl

#
#	Unpack an OpenDocument file to a directory.

#
#	Archive::Zip is used to unzip files.
#	File::Path is used to create and remove directories.
#
use Archive::Zip;
use File::Path;
use strict;

my $file_name;
my $dir_name;
my $suffix;
my $zip;
my $member_name;
my @member_list;

if (scalar @ARGV != 1)
{
	print "Usage: $0 filename\n";
	exit;
}

$file_name = $ARGV[0];

#
#	Only allow filenames that have valid OpenDocument extensions
#
if ($file_name =~ m/\.(o[dt][tgpscif]|odm|oth)/)
{
	$suffix = $1;
	
	#
	#	Create directory name based on filename
	#
	($dir_name = $file_name) =~ s/\.$suffix//;
	$dir_name .= "_$suffix";
	
	#
	#	Forcibly remove old directory, re-create it,
	#	and unzip the OpenOffice.org file into that directory
	#
	rmtree($dir_name, 0, 0);
	mkpath($dir_name, 0, 0755);
	
	$zip = Archive::Zip->new( $file_name );
	@member_list = $zip->memberNames( );
	
	foreach $member_name (@member_list)
	{
		$zip->extractMember( $member_name, "$dir_name/$member_name" );
	}
	
	print "$file_name unpacked.\n";
}
else
{
	print "This does not appear to be an OpenDocument file.\n";
	print "Legal suffixes are .odt, .ott, .odg, .otg, .odp, .otp,\n";
	print ".ods, .ots, .odc, .otc, .odi, .oti, .odf, .otf, .odm, and .oth\n";
}
