#!/usr/bin/perl

use Archive::Zip;
use XML::DOM;
use warnings;
use strict;

#
#	Command line arguments:
#		input file name
#		output file name

my $doc;		# the DOM document
my $rows;		# all the <table:table-row> elements
my $n_rows;		# number of rows
my $row;		# current row number
my $col;		# current column number
my @data;		# contents of current row
my $sum;		# sum of the row items
my @legends;	# legends for the graph

my $main_handle;		# content/style file handle
my $main_filename;		# content/style file name

my $manifest_handle;	# manifest file handle
my $manifest_filename;	# manifest file name

my $chart_handle;		# chart file handle
my $chart_filename;		# chart file name

my @temp_filename;		# list of all temporary filenames created
my $item;				# foreach loop variable

my $zip;				# output zip file name

my $percent;	# string holding nicely formatted percent value

if (scalar @ARGV != 2)
{
	print "Usage: $0 inputfile outputfile\n";
	exit;
}

print "Processing $ARGV[0]\n";

$doc = makeDOM( $ARGV[0] );

$zip = Archive::Zip->new();

($main_handle, $main_filename) = Archive::Zip->tempFile();
push @temp_filename, $main_filename;

print $main_handle <<"STYLEINFO";
<office:document-styles
  xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
  xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
  xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0"
  xmlns:table="urn:oasis:names:tc:opendocument:xmlns:table:1.0"
  xmlns:draw="urn:oasis:names:tc:opendocument:xmlns:drawing:1.0"
  xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0"
  xmlns:xlink="http://www.w3.org/1999/xlink"
  xmlns:number="urn:oasis:names:tc:opendocument:xmlns:datastyle:1.0"
  xmlns:svg="urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0"
  xmlns:chart="urn:oasis:names:tc:opendocument:xmlns:chart:1.0"
  xmlns:dr3d="urn:oasis:names:tc:opendocument:xmlns:dr3d:1.0"
  xmlns:math="http://www.w3.org/1998/Math/MathML"
  xmlns:script="urn:oasis:names:tc:opendocument:xmlns:script:1.0"
  xmlns:dom="http://www.w3.org/2001/xml-events"
  office:version="1.0">

	<office:automatic-styles>
		<style:page-layout style:name="pm1">
			<style:page-layout-properties
			  fo:page-width="21.59cm" fo:page-height="27.94cm" 
			  style:num-format="1" style:print-orientation="portrait" 
			  fo:margin-top="1.27cm" fo:margin-bottom="1.27cm" 	
			  fo:margin-left="1.27cm" fo:margin-right="1.27cm" 
			  style:writing-mode="lr-tb" style:footnote-max-height="0cm">
    			<style:columns fo:column-count="0" fo:column-gap="0cm"/>
			</style:page-layout-properties>
			<style:header-style/>
			<style:footer-style/>
		</style:page-layout>
	</office:automatic-styles>
	<office:master-styles>
		<style:master-page style:name="Standard" style:page-layout-name="pm1"/>
	</office:master-styles>
</office:document-styles>
STYLEINFO

close $main_handle;
$zip->addFile( $main_filename, "styles.xml" );

#
#	Create manifest file and its boilerplate
#

($manifest_handle, $manifest_filename) = Archive::Zip->tempFile();
push @temp_filename, $manifest_filename;

print $manifest_handle <<"MANIFEST_HEADER";
<!DOCTYPE manifest:manifest
	PUBLIC "-//OpenOffice.org//DTD Manifest 1.0//EN" "Manifest.dtd">
<manifest:manifest 
  xmlns:manifest="urn:oasis:names:tc:opendocument:xmlns:manifest:1.0">
	<manifest:file-entry
		manifest:media-type="application/vnd.oasis.opendocument.text"
		manifest:full-path="/"/>
	<manifest:file-entry
		manifest:media-type="text/xml" manifest:full-path="content.xml"/>
	<manifest:file-entry
		manifest:media-type="text/xml" manifest:full-path="styles.xml"/>
MANIFEST_HEADER

#
#	Create the main content.xml file and its
#	header information
#
($main_handle, $main_filename) = Archive::Zip->tempFile();
push @temp_filename, $main_filename;

print $main_handle  <<"CONTENT_HEADER";
<?xml version="1.0" encoding="UTF-8"?>
<office:document-content
  xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
  xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
  xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0"
  xmlns:table="urn:oasis:names:tc:opendocument:xmlns:table:1.0"
  xmlns:draw="urn:oasis:names:tc:opendocument:xmlns:drawing:1.0"
  xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0"
  xmlns:xlink="http://www.w3.org/1999/xlink"
  xmlns:number="urn:oasis:names:tc:opendocument:xmlns:datastyle:1.0"
  xmlns:svg="urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0"
  xmlns:chart="urn:oasis:names:tc:opendocument:xmlns:chart:1.0"
  xmlns:script="urn:oasis:names:tc:opendocument:xmlns:script:1.0"
  office:version="1.0">
	<office:scripts/>

  	<office:font-face-decls>
		<style:font-face style:name="Bitstream Charter"
			svg:font-family="&apos;Bitstream Charter&apos;"
			style:font-pitch="variable"/>
	</office:font-face-decls>

	<office:automatic-styles>
		<!-- style for question title -->
		<style:style style:name="hdr1" style:family="paragraph">
            <style:text-properties
			style:font-name="Bitstream Charter"
			style:font-family-generic="roman"
			style:font-pitch="variable"
            fo:font-size="14pt"
            fo:font-style="italic"/>
    	</style:style>

		<!-- style for text summary of results -->
		<style:style style:name="info" style:family="paragraph">
            <style:paragraph-properties>
				<style:tab-stops>
            		<style:tab-stop style:position="3.5cm"
						style:type="right"/>
            		<style:tab-stop style:position="5cm"
						style:type="char"
                		style:char="."/>
				</style:tab-stops>
			</style:paragraph-properties>
			<style:text-properties
				style:font-name="Bitstream Charter"
				style:font-family-generic="roman"
				style:font-pitch="variable"
				fo:font-size="10pt"/>
    	</style:style>

		<!-- style to force a move to column two -->
		<style:style style:name="colBreak" style:family="paragraph">
			<style:paragraph-properties fo:break-before="column"/>
		</style:style>

		<!-- set column widths -->
		<style:style style:name="Sect1" style:family="section">
			<style:section-properties
				text:dont-balance-text-columns="true">
				<style:columns fo:column-count="2">
					<style:column style:rel-width="5669*"
						fo:margin-left="0cm" fo:margin-right="0cm"/>
					<style:column style:rel-width="5131*"
						fo:margin-left="0cm" fo:margin-right="0cm"/>
				</style:columns>
			</style:section-properties>
		</style:style>

		<!-- style for chart frame -->
		<style:style style:name="fr1" style:family="graphic">
			<style:graphic-properties style:wrap="run-through"
				style:vertical-pos="middle"
				style:horizontal-pos="from-left"/>
		</style:style>
	</office:automatic-styles>

	<office:body>
		<office:text>
CONTENT_HEADER

$rows = $doc->getElementsByTagName( "table:table-row" );
getRowContents( \@legends, $rows->item(0));

$n_rows = $rows->getLength;

for ($row=1; $row<$n_rows; $row++)
{
	getRowContents( \@data, $rows->item($row));

	next if (!$data[0]);  # skip rows without a question
	
	$sum = 0;
	for ($col=1; $col < scalar(@data); $col++)
	{
		$sum += $data[$col];
	}
	
	print $main_handle qq!<text:section text:style-name="Sect1"!;
	print $main_handle qq! text:name="Section$row">!;
	print $main_handle qq!<text:h text:style-name="hdr1" text:outline-level="1">!;
	print $main_handle qq!$row. $data[0]</text:h>\n!;
	for ($col=1; $col < scalar(@data); $col++)
	{
		$percent = sprintf(" (%.2f%%)", 100*$data[$col]/$sum);
		print $main_handle qq!<text:p text:style-name="info">!;
		print $main_handle qq!$legends[$col]<text:tab/>$data[$col]!;
		print $main_handle qq!<text:tab/>$percent</text:p>\n!;
	}

	# now insert the reference to the graph
	
	print $main_handle qq!<text:p text:style-name="colBreak">!;
	print $main_handle qq!<draw:frame draw:style-name="fr1"
		draw:name="Object$row" draw:layer="layout"
		svg:width="8cm" svg:height="7cm"><draw:object
		xlink:href="./Object$row" xlink:type="simple"
		xlink:show="embed" xlink:actuate="onLoad"/></draw:frame>\n!;
	print $main_handle qq!</text:p>\n!;
	print $main_handle qq!</text:section>\n!;
	
	construct_chart( \@legends, \@data, $row );
	
	append_manifest( $row );
}


print $main_handle <<"CONTENT_FOOTER";
		</office:text>
	</office:body>
</office:document-content>
CONTENT_FOOTER

close $main_handle;

print $manifest_handle "</manifest:manifest>\n";
close $manifest_handle;
$zip->addFile( $manifest_filename, "META-INF/manifest.xml");
$zip->addFile( $main_filename, "content.xml");
$zip->writeToFileNamed( $ARGV[1] );

foreach $item (@temp_filename)
{
	unlink $item;
}

#==================== end main program =======================

#
#	Append data to the manifest file;
#	the parameter is the chart number
#
sub append_manifest
{
	my $number = shift;
	
	print $manifest_handle <<"ADD_MANIFEST";
<manifest:file-entry
	manifest:media-type="application/vnd.oasis.opendocument.chart"
	manifest:full-path="Object$number/"/>
<manifest:file-entry
	manifest:media-type="text/xml"
	manifest:full-path="Object$number/content.xml"/>

ADD_MANIFEST
}

#
#	Construct the chart file, given:
#		reference to the @legends array
#		reference to the @data array
#		chart number
#
sub construct_chart
{
	my $legendref = shift;
	my $dataref = shift;
	my $chart_num = shift;
	
	my $cell;	# current cell number being processed
	
	($chart_handle, $chart_filename) = Archive::Zip->tempFile();
	push @temp_filename, $chart_filename;
	print $chart_handle <<"CHART_HEADER";
<office:document-content
  xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
  xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
  xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0"
  xmlns:table="urn:oasis:names:tc:opendocument:xmlns:table:1.0"
  xmlns:draw="urn:oasis:names:tc:opendocument:xmlns:drawing:1.0"
  xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0"
  xmlns:xlink="http://www.w3.org/1999/xlink" 
  xmlns:number="urn:oasis:names:tc:opendocument:xmlns:datastyle:1.0"
  xmlns:svg="urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0"
  xmlns:chart="urn:oasis:names:tc:opendocument:xmlns:chart:1.0"
  office:version="1.0">
	<office:automatic-styles>
		<number:number-style style:name="N0">
			<number:number number:min-integer-digits="1"/>
		</number:number-style>
		<style:style style:name="main" style:family="chart">
			<style:graphic-properties draw:stroke="none"
				svg:stroke-width="0cm" svg:stroke-color="#000000"
				draw:fill-color="#ffffff"/>
		</style:style>
		<style:style style:name="title" style:family="chart">
			<style:text-properties
				style:font-family-generic="swiss"
				fo:font-size="12pt"/>
		</style:style>
		<style:style style:name="legend" style:family="chart">
			<style:text-properties
				style:font-family-generic="swiss"
				fo:font-size="8pt"/>
		</style:style>
		<style:style style:name="plot" style:family="chart">
			<style:chart-properties
				chart:lines="false"
				chart:series-source="columns"/>
		</style:style>
		<style:style style:name="axis" style:family="chart"
			style:data-style-name="N0">
			<style:chart-properties chart:display-label="true"/>
		</style:style>
		<style:style style:name="series" style:family="chart">
			<style:graphic-properties draw:fill-color="#ffffff"/>
		</style:style>
		
		<style:style style:name="slice1" style:family="chart">
			<style:graphic-properties draw:fill-color="#ff6060"/>
		</style:style>
		<style:style style:name="slice2" style:family="chart">
			<style:graphic-properties draw:fill-color="#ffa560"/>
		</style:style>
		<style:style style:name="slice3" style:family="chart">
			<style:graphic-properties draw:fill-color="#ffff60"/>
		</style:style>
		<style:style style:name="slice4" style:family="chart">
			<style:graphic-properties draw:fill-color="#60ff60"/>
		</style:style>
		<style:style style:name="slice5" style:family="chart">
			<style:graphic-properties draw:fill-color="#6060ff"/>
		</style:style>
		<style:style style:name="slice6" style:family="chart">
			<style:graphic-properties draw:fill-color="#606080"/>
		</style:style>
	</office:automatic-styles>

	<office:body>
	  <office:chart>
		<chart:chart chart:class="chart:circle" chart:style-name="main"
		  svg:width="9cm" svg:height="9cm">
		<chart:title chart:style-name="title" svg:x="1cm"
			svg:y="0.25cm">
			<text:p>${$dataref}[0]</text:p>
		</chart:title>
		<chart:legend chart:legend-position="end" svg:x="8cm" svg:y="3cm"
			chart:style-name="legend"/>
		
		<chart:plot-area svg:x="0.5cm" svg:y="1.5cm"
			svg:width="6cm" svg:height="6cm" chart:style-name="plot">
			<chart:axis chart:dimension="y" chart:style-name="axis"
				chart:name="primary-y">
				<chart:grid chart:class="major"/>
			</chart:axis>
			<chart:series chart:style-name="series"
				chart:values-cell-range-address="local-table.B2:.B7"
				chart:label-cell-address="local-table.B1">
				<chart:data-point chart:style-name="slice1"/>
				<chart:data-point chart:style-name="slice2"/>
				<chart:data-point chart:style-name="slice3"/>
				<chart:data-point chart:style-name="slice4"/>
				<chart:data-point chart:style-name="slice5"/>
				<chart:data-point chart:style-name="slice6"/>
			</chart:series>
		</chart:plot-area>
		<table:table table:name="local-table">
    		<table:table-header-columns>
        		<table:table-column/>
    		</table:table-header-columns>
    		<table:table-columns>
        		<table:table-column table:number-columns-repeated="2"/>
    		</table:table-columns>
			
			<table:table-header-rows>
				<table:table-row>
					<table:table-cell><text:p/></table:table-cell>
					<table:table-cell office:value-type="string">
						<text:p>N</text:p>
					</table:table-cell>
				</table:table-row>
			</table:table-header-rows>
			<table:table-rows>
CHART_HEADER
	for ($cell=1; $cell < scalar(@{$dataref}); $cell++)
	{
		print $chart_handle qq!<table:table-row>\n!;
		print $chart_handle qq!<table:table-cell office:value-type="string">!;
		print $chart_handle qq!<text:p>!, ${$legendref}[$cell], qq!</text:p>!;
		print $chart_handle qq!</table:table-cell>!;
		print $chart_handle qq!<table:table-cell office:value-type="float" !;
		print $chart_handle qq!office:value="!, ${$dataref}[$cell], qq!">!;
		print $chart_handle qq!<text:p>!, ${$dataref}[$cell], qq!</text:p>!;
		print $chart_handle qq!</table:table-cell></table:table-row>\n!;
	}
	print $chart_handle <<"CHART_FOOTER";
</table:table-rows>
</table:table>
</chart:chart>
</office:chart>
</office:body>
</office:document-content>
CHART_FOOTER
	close $chart_handle;
	$zip->addFile( $chart_filename, "Object$row/content.xml" );
}

#
#	Extract the content.xml file from the given
#	filename, parse it, and return a DOM object.
#
sub makeDOM
{
	my ($filename) = shift;
	my $input_zip = Archive::Zip->new( $filename );
	my $parser = new XML::DOM::Parser;
	my $doc;
	my $temp_handle;
	my $temp_filename;
	
	($temp_handle, $temp_filename) = Archive::Zip->tempFile();

	$input_zip->extractMember( "content.xml", $temp_filename );

	$doc = $parser->parsefile( $temp_filename );
	unlink $temp_filename;
	return $doc;
}

#
#	$node - starting node
#	$name - name of desired child element
#	returns the node's first child with the given name
#
sub getFirstChildElement
{
	my ($node, $name) = @_;
	for my $child ($node->getChildNodes)
	{
		if ($child->getNodeName eq $name)
		{
			return $child;
		}
	}
	return undef;
}

#
#	$node - starting node
#	$name - name of desired sibling element
#	returns the node's next sibling with the given name
#
sub getNextSiblingElement
{
	my ($node, $name) = @_;
	
	while (($node = $node->getNextSibling) &&
		$node->getNodeName ne $name)
	{
		# do nothing
		;
	}
	
	return $node;
}

#
#	$itemref - Reference to an array to hold the row contents
#	$rowNode - a table row
#
sub getRowContents
{
	my ($itemRef, $rowNode) = @_;
	my $cell;			# a cell node
	my $value;
	my $n_repeat;
	my $i;
	my $para;	# <text:p> node

	@{$itemRef} = ();
	$cell = getFirstChildElement( $rowNode, "table:table-cell" );
	while ($cell)
	{
		$n_repeat = $cell->getAttribute("table:number-columns-repeated");
		$n_repeat = 1 if (!$n_repeat);
		
		$value = "";
		$para = getFirstChildElement( $cell, "text:p" );
		while ($para)
		{
			$value .= $para->getFirstChild->getNodeValue . " ";
			$para = getNextSiblingElement( $para, "text:p" );
		}
		chop $value;
		for ($i=0; $i < $n_repeat; $i++)
		{
			push @{$itemRef}, $value;
		}
		$cell = getNextSiblingElement( $cell, "table:table-cell" );
	}
}
