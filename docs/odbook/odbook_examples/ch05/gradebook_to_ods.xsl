<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
    xmlns:meta="urn:oasis:names:tc:opendocument:xmlns:meta:1.0"
    xmlns:config="urn:oasis:names:tc:opendocument:xmlns:config:1.0"
    xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0"
    xmlns:table="urn:oasis:names:tc:opendocument:xmlns:table:1.0"
    xmlns:draw="urn:oasis:names:tc:opendocument:xmlns:drawing:1.0"
    xmlns:presentation="urn:oasis:names:tc:opendocument:xmlns:presentation:1.0"
    xmlns:dr3d="urn:oasis:names:tc:opendocument:xmlns:dr3d:1.0"
    xmlns:chart="urn:oasis:names:tc:opendocument:xmlns:chart:1.0"
    xmlns:form="urn:oasis:names:tc:opendocument:xmlns:form:1.0"
    xmlns:script="urn:oasis:names:tc:opendocument:xmlns:script:1.0"
    xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
    xmlns:number="urn:oasis:names:tc:opendocument:xmlns:datastyle:1.0"
    xmlns:anim="urn:oasis:names:tc:opendocument:xmlns:animation:1.0"

    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:math="http://www.w3.org/1998/Math/MathML"
    xmlns:xforms="http://www.w3.org/2002/xforms"

    xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0"
    xmlns:svg="urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0"
    xmlns:smil="urn:oasis:names:tc:opendocument:xmlns:smil-compatible:1.0"
	
	xmlns:ooo="http://openoffice.org/2004/office"
	xmlns:ooow="http://openoffice.org/2004/writer"
	xmlns:oooc="http://openoffice.org/2004/calc" 
>

<xsl:output method="xml" indent="yes"/>

<xsl:include href="fontdecls.xsl"/>

<xsl:param name="courseName">Course</xsl:param>

<xsl:key name="student-index" match="student" use="@id"/>

<xsl:template match="/">
<office:document-content office:version="1.0">
	
<office:scripts/>
	
<xsl:call-template name="insert-font-decls"/>

<office:automatic-styles>

	<!-- Column for last and first name -->
	<style:style style:name="co1" style:family="table-column">
		<style:table-column-properties fo:break-before="auto"
			style:column-width="2.5cm"/>
	</style:style>

	<!-- column for final grade and percentage -->
	<style:style style:name="co2" style:family="table-column">
		<style:table-column-properties fo:break-before="auto"
			style:column-width="2cm"/>
	</style:style>

	<!-- All other columns -->
	<style:style style:name="co3" style:family="table-column">
		<style:table-column-properties fo:break-before="auto"
			style:column-width="1.25cm"/>
	</style:style>
		
	<!-- Let all the rows have optimal height -->
	<style:style style:name="ro1" style:family="table-row">
		<style:table-row-properties fo:break-before="auto"
			style:use-optimal-row-height="true"/>
	</style:style>
	
	<!-- The table references a master-page which doesn't exist,
		but that doesn't bother OpenOffice.org -->
	<style:style style:name="ta1" style:family="table" 
		style:master-page-name="TAB_Sheet1">
		<style:table-properties table:display="true"/>
	</style:style>

	<!-- Number style for a percentage -->
	<number:percentage-style style:name="N01">
		<number:number number:decimal-places="2"
			number:min-integer-digits="1"/>
		<number:text>%</number:text>
	</number:percentage-style>
	
	<!-- individual cell styles -->
	
	<!-- style for final grade letter -->
	<style:style style:name="centered" style:family="table-cell"
		style:parent-style-name="Default">
		<style:paragraph-properties fo:text-align="center"/>
	</style:style>
	
	<!-- style for the total grade percent -->
	<style:style style:name="percent" style:family="table-cell"
		style:data-style-name="N01"/>
	
	<!-- style for heading cells -->
	<style:style style:name="heading" style:family="table-cell"
		style:parent-style-name="Default">
		<style:paragraph-properties fo:text-align="center"/>
		<style:text-properties fo:font-weight="bold"/>
	</style:style>
	
	<!-- style for raw data cells (just use OpenOffice.org defaults) -->
	<style:style style:name="normal" style:family="table-cell"
		style:parent-style-name="Default"/>
</office:automatic-styles>

<office:body>
	<office:spreadsheet>
	
		<!-- calculate number of raw data columns -->
		<xsl:variable name="numTasks"
			select="count(gradebook/task-list/task[@recorded='yes'])"/>
		
		<!-- start the spreadsheet -->
		<table:table table:name="{$courseName} Final Grades"
			table:style-name="ta1">
		
			<!-- last name, first name, and ID are repeated when printing -->
			<table:table-header-columns>
				<table:table-column table:style-name="co1"
					table:default-cell-style-name="normal"/>
				<table:table-column table:style-name="co1"
					table:default-cell-style-name="normal"/>
				<table:table-column table:style-name="co1"
					table:default-cell-style-name="normal"/>
			</table:table-header-columns>
			
			<!-- final grade -->
			<table:table-column table:style-name="co2"
				table:default-cell-style-name="centered" />
			
			<!-- percentage -->
			<table:table-column table:style-name="co2"
				table:default-cell-style-name="percent" />
			
			<!-- everyone else -->
			<table:table-column table:style-name="co3"
				table:default-cell-style-name="normal"
				table:number-columns-repeated="{$numTasks}">
			</table:table-column>
	
			<!-- Now begins the actual data, starting with a header row
				to be repeated when printing -->
			<table:table-header-rows>
				<table:table-row table:style-name="ro1">
					<table:table-cell table:style-name="heading">
						<text:p>Last Name</text:p>
					</table:table-cell>
	
					<table:table-cell table:style-name="heading">
						<text:p>First Name</text:p>
					</table:table-cell>
	
					<table:table-cell table:style-name="heading">
						<text:p>ID</text:p>
					</table:table-cell>
	
					<table:table-cell table:style-name="heading">
						<text:p>Grade</text:p>
					</table:table-cell>
	
					<table:table-cell table:style-name="heading">
						<text:p>Total %</text:p>
					</table:table-cell>
	
					<!-- emit heading cell for each recorded task -->
					<xsl:for-each
						select="gradebook/task-list/task[@recorded='yes']">
						<xsl:sort select="@date" order="descending"/>
							<table:table-cell table:style-name="heading">
								<text:p><xsl:value-of select="@id"/></text:p>
							</table:table-cell>
					</xsl:for-each>
				</table:table-row>
			</table:table-header-rows>
	
			<!-- Now create the rows with the weights -->
	
			<table:table-row table:style-name="ro1">
				<!-- five empty cells -->
				<table:table-cell table:number-columns-repeated="5"/>
				<xsl:for-each select="gradebook/task-list/task[@recorded='yes']">
					<xsl:sort select="@date" order="descending"/>
					<xsl:variable name="factor" select="@weight div @max"/>
					<table:table-cell office:value-type="float"
						office:value="{$factor}">
						<text:p><xsl:value-of select="$factor"/></text:p>
					</table:table-cell>
				</xsl:for-each>
			</table:table-row>
	
			<!-- Create one row for each student -->
			<xsl:apply-templates select="gradebook/student">
				<xsl:sort select="last"/>
				<xsl:sort select="first"/>
				<xsl:with-param name="numCols" select="$numTasks+5"/>
			</xsl:apply-templates>
		</table:table>
	</office:spreadsheet>
</office:body>
</office:document-content>
</xsl:template>


<xsl:template match="student">
	<xsl:param name="numCols"/>
	
	<!-- generate the column letter for the last column -->
	<xsl:variable name="lastCol">
		<xsl:call-template name="generate-col">
			<xsl:with-param name="n" select="$numCols"/>
		</xsl:call-template>
	</xsl:variable>
	
	<table:table-row>
	
	<!-- cells for last name, first name, and student ID -->
	<table:table-cell>
		<text:p><xsl:value-of select="last"/></text:p>
	</table:table-cell>
	<table:table-cell>
		<text:p><xsl:value-of select="first"/></text:p>
	</table:table-cell>
	<table:table-cell>
		<text:p><xsl:value-of select="substring(@id,2)"/></text:p>
	</table:table-cell>
	
	<!-- formula for final grade letter -->
	<table:table-cell
		table:formula="oooc:=MID(&quot;FFFFFFDCBAA&quot;;INT([.E{position()+2}]*10)+1;1)"/>

	<!-- formula for final grade as a percentage -->
	<table:table-cell
		table:number-matrix-columns-spanned="1"
		table:number-matrix-rows-spanned="1"
		table:formula="oooc:=SUM([.F{position()+2}:.{$lastCol}{position()+2}]*[.$F$2:.${$lastCol}$2])/100" office:value-type="float"/>


	<!-- save the student's id -->
	<xsl:variable name="id" select="@id"/>

	<!-- insert a cell for each recorded score -->
	<xsl:for-each select="/gradebook/task-list/task[@recorded='yes']">
		<xsl:sort select="@date" order="descending"/>
		<xsl:variable name="taskID" select="@id"/>
		<xsl:call-template name="insert_score">
			<xsl:with-param name="n"
				select="key('student-index', $id)/result[@ref=$taskID]/@score"/>
		</xsl:call-template>
	</xsl:for-each>
	</table:table-row>
</xsl:template>


<xsl:template name="insert_score">
	<xsl:param name="n"/>
	
	<xsl:choose>
	<xsl:when test="$n != ''">
		<table:table-cell office:value-type="float" office:value="{$n}">
			<text:p><xsl:value-of select="$n"/></text:p>
		</table:table-cell>
	</xsl:when>
	<xsl:otherwise>
		<table:table-cell><text:p/></table:table-cell>
	</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="generate-col">
	<xsl:param name="n"/>
	<xsl:variable name="letters">ABCDEFGHIJKLMNOPQRSTUVWXYZ</xsl:variable>
	<xsl:choose>
		<xsl:when test="$n &lt;= 26">
			<xsl:value-of select="substring($letters, $n, 1)"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="concat(
				substring($letters, floor(($n - 1) div 26) + 1, 1),
				substring($letters, (($n - 1) mod 26) + 1, 1))"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

</xsl:stylesheet>
