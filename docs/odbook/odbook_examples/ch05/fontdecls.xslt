<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:office="http://openoffice.org/2000/office"
    xmlns:style="http://openoffice.org/2000/style"
    xmlns:fo="http://www.w3.org/1999/XSL/Format">

<xsl:template name="insert-font-decls">
    <office:font-decls>
        <style:font-decl style:name="Arial Unicode MS"
            fo:font-family="&#8217;Arial Unicode MS&#8217;"
        style:font-pitch="variable" />
        <style:font-decl style:name="Palatino"
            fo:font-family="Palatino" style:font-family-generic="roman"
        style:font-pitch="variable" />
        <style:font-decl style:name="Chancery L"
            fo:font-family="&#8217;Chancery L&#8217;" />
        <style:font-decl style:name="Courier"
            fo:font-family="Courier" style:font-family-generic="modern"
        style:font-pitch="fixed" />
    </office:font-decls>
</xsl:template>
</xsl:stylesheet>

