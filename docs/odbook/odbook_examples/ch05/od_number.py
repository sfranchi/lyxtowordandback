import xml.dom
import xml.dom.ext
import xml.dom.minidom
import re
from xml.dom.NodeFilter import NodeFilter

class ODNumber:
	def __init__(self, document, formatString, language, country,
	currencySymbol ):
		self.thousands = False;		# thousands separator?
		self.nDecimals = 0;			# number of decimal places
		self.minIntegerDigits = 0;	# min. integer digits
		self.textStr = ""			# text string being built
		self.fragment = None		# fragment being built
		
		self.document = document	# copy parameters to class attributes
		self.language = language
		self.country = country
		self.currencySymbol = currencySymbol
		self.formatString = formatString
		

	def endStr( self ):
		"""Output any accumulated text
		
		Creates a text node for any text that has been accumulated
		into textStr
		"""
		if (self.textStr != ""):
			textElement = self.document.createElement( "number:text" )
			textNode = self.document.createTextNode( self.textStr )
			textElement.appendChild( textNode )
			self.fragment.appendChild( textElement )
			self.textStr = ""

	def addCurrency( self ):
		self.endStr()
		node = self.fragment.appendChild(
			self.document.createElement( "number:currency-symbol" ) )
		node.setAttribute( "number:language", self.language )
		node.setAttribute( "number:country", self.country )
		node.appendChild( self.document.createTextNode( self.currencySymbol ) )

	def addNumber( self ):
		node = self.fragment.appendChild(
			self.document.createElement("number:number") )
		node.setAttribute( "number:min-integer-digits",
			"%d" % self.minIntegerDigits )
		if (self.nDecimals > 0):
			node.setAttribute( "number:decimal-places",
				"%d" % self.nDecimals )
		if (self.thousands):
			node.setAttribute( "number:grouping", "true" )

	def createCurrencyStyle ( self ):
		"""Scan a format string, where:

		$ indicates the currency symbol
		# indicates an optional digit
		0 indicates a required digit
		, indicates the thousands separator (no matter your locale)
		. indicates the decimal point (no matter your locale)

		Creates a document fragment with appropriate OpenDocument
		markup.
		"""

		self.fragment = self.document.createElement("number:fragment")

		hasDecimal = False

		numchars = re.compile( "[#,0.]" )
		i = 0
		while (i < len(self.formatString)):
			char = self.formatString[i]
			if (char == "$"):
				self.addCurrency( )
			elif (re.search( numchars, char )):
				self.endStr( )
				while (re.search( numchars, char )):
					if (char == ","):
						self.thousands = True
					elif (char == "0"):
						if (hasDecimal):
							self.nDecimals = self.nDecimals + 1
						else:
							self.minIntegerDigits = \
								self.minIntegerDigits + 1
					elif (char == "."):
						hasDecimal = True;

					if (i == len(self.formatString) - 1):
						break;
					i = i + 1
					char = self.formatString[i]
				self.addNumber( )
			else:
				self.textStr = self.textStr + char
			i = i + 1
		self.endStr( )

	def getFragment( self ):
		return self.fragment

	def showFragment( self, node, level ):
		if (node.nodeType == xml.dom.Node.ELEMENT_NODE):
				print " " * level, node.tagName
				if (node.hasChildNodes):
					for child in node.childNodes:
						self.showFragment( child, level+1 )
		elif (node.nodeType == xml.dom.Node.TEXT_NODE):
				print " " * level, node.nodeValue
