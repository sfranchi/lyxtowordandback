import xml.dom
import xml.dom.ext
import xml.dom.minidom
import xml.parsers.expat
import sys
import od_number
from zipfile import *
from StringIO import *

def getChildElement( node, name ):
	"""Get first <name> child of the given node

	Because of whitespace text nodes, the first child of a node
	may not be an element node. This function looks only at
	element nodes when finding the first child.
	"""
	node = node.firstChild;
	while (node != None):
		if (node.nodeType != xml.dom.Node.ELEMENT_NODE or
	  	  node.nodeName != name):
			node = node.nextSibling
		else:
			break
	return node

def getSiblingElement( node, name ):
	"""Get next <name> sibling of the given node

	Because of whitespace text nodes, the next sibling of a node
	may not be an element node. This function looks only at
	element nodes when finding the first sibling.
	"""
	node = node.nextSibling;
	while (node != None):
		if (node.nodeType != xml.dom.Node.ELEMENT_NODE or
	  	  node.nodeName != name):
			node = node.nextSibling
		else:
			break
	return node

def copyManifest():
	#
	#	read the manifest.xml file as a string
	#	and create a disk file for transfer to the .zip output
	dataSource = inFile.read( "META-INF/manifest.xml" )
	tempFileName = "/tmp/workfile"
	dataSink = open(tempFileName, "w")
	dataSink.write( dataSource );
	dataSink.close();
	outFile.write( tempFileName, "META-INF/manifest.xml" )

def fixCurrency( filename ):

	#
	#	Read the styles.xml file as a string file
	#	and create a disk file for output
	#
	dataSource = StringIO (inFile.read( filename ))
	tempFileName = "/tmp/workfile"
	dataSink = open(tempFileName, "w")

	#
	#	Parse the document
	#
	document = xml.dom.minidom.parse( dataSource )

	#
	#	Create document fragments from the format strings
	#
	posXML = od_number.ODNumber( document, positiveFormatString, 
		language, country, currencySymbol )
	posXML.createCurrencyStyle( )
	
	negXML = od_number.ODNumber( document, negativeFormatString,
		language, country, currencySymbol )
	negXML.createCurrencyStyle( )

	#
	#	Fix number style elements
	#
	currencyElements = document.getElementsByTagName("number:currency-symbol")
	for element in currencyElements:
		if (element.getAttribute( "number:language" ) == oldLanguage and
			element.getAttribute( "number:country" ) == oldCountry):
			element.setAttribute( "number:language", language )
			element.setAttribute( "number:country", country )

			parent = element.parentNode
			children = parent.childNodes

			i = len(children)-1
			while (i >= 0):
				if (children[i].nodeName == "number:number" or 
				children[i].nodeName == "number:text" or 
				children[i].nodeName == "number:currency-symbol" or 
				children[i].nodeType == xml.dom.Node.TEXT_NODE):
					parent.removeChild( children[i] )
				i = i - 1

			#	select the appropriate number format markup
			if ((parent.getAttribute("style:name"))[-2:] == "P0"):
				fragment = posXML.getFragment()
			else:
				fragment = negXML.getFragment()
			#
			#	and insert it into the <number:currency-style> element
			for child in fragment.childNodes:
				parent.appendChild( child.cloneNode(True) )

	
	#
	#	Fix table cells (which only exist in content.xml)
	#
	rowElements = document.getElementsByTagName("table:table-row")
	for row in rowElements:
		cell = getChildElement( row, "table:table-cell" )
		while (cell != None):
			if (cell.getAttribute("office:currency") ==  oldAbbrev ):
				cell.setAttribute("office:currency", abbreviation )

				# change the currency abbreviation
				valueStr = cell.getAttribute("office:value")

				# and the number in the cell, if there is one
				if (valueStr != ""):
					result = float( valueStr ) * factor
					cell.setAttribute("office:value",  '%f' % result)
				
				#	remove any children of this cell
				children = cell.childNodes
				i = len(children)-1
				while (i >= 0):
					cell.removeChild( children[i] )
					i = i - 1

			# move to the next cell in the row		
			cell = getSiblingElement( cell, "table:table-cell" )

	#
	#	Serialize the document tree to the output file
	xml.dom.ext.Print( document, dataSink )
	dataSink.close();

	#
	#	Add the temporary file to the new .zip file with the correct name.
	#
	outFile.write( tempFileName, filename )

def getParameters( filename ):
	global oldLanguage, oldCountry, oldAbbrev 
	global language, country, abbreviation, currencySymbol 
	global positiveFormatString, negativeFormatString, factor

	paramFile = open( filename, "r" )
	document = xml.dom.minidom.parse( paramFile )
	node = document.getElementsByTagName( "from" )[0]
	oldLanguage = node.getAttribute( "language" )
	oldCountry = node.getAttribute( "country" )
	oldAbbrev = node.getAttribute( "abbrev" )
	
	node = document.getElementsByTagName( "to" )[0]
	language = node.getAttribute( "language" )
	country = node.getAttribute( "country" )
	abbreviation = node.getAttribute( "abbrev" )
	currencySymbol = node.getAttribute( "symbol" )
	positiveFormatString = node.getAttribute( "positiveFormat" )
	negativeFormatString = node.getAttribute( "negativeFormat" )
	factor = float( node.getAttribute("factor") )
	
	paramFile.close()
		
if (len(sys.argv) == 4):
	
	#	Open an existing OpenDocument file
	#
	inFile = ZipFile( sys.argv[1] )
	
	#	...and a brand new output file
	#
	outFile = ZipFile( sys.argv[2], "w", ZIP_DEFLATED );
	
	getParameters( sys.argv[3] )
	
	#
	#	modify all appropriate currency styles
	#
	fixCurrency( "styles.xml" )
	fixCurrency( "content.xml" )
	
	#
	#	copy the manifest
	#
	copyManifest( )

	inFile.close
	outFile.close
else:
	print "Usage: " + sys.argv[0] + " inputfile outputfile parameterfile"
