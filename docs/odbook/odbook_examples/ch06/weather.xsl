<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:config="http://openoffice.org/2001/config" 
    xmlns:dc="http://purl.org/dc/elements/1.1/" 
    xmlns:draw="http://openoffice.org/2000/drawing" 
    xmlns:event="http://openoffice.org/2001/event" 
    xmlns:fo="http://www.w3.org/1999/XSL/Format" 
    xmlns:image="http://openoffice.org/2001/image" 
    xmlns:meta="http://openoffice.org/2000/meta" 
    xmlns:number="http://openoffice.org/2000/datastyle" 
    xmlns:office="http://openoffice.org/2000/office" 
    xmlns:script="http://openoffice.org/2000/script" 
    xmlns:statusbar="http://openoffice.org/2001/statusbar" 
    xmlns:style="http://openoffice.org/2000/style" 
    xmlns:svg="http://www.w3.org/2000/svg" 
    xmlns:table="http://openoffice.org/2000/table" 
    xmlns:text="http://openoffice.org/2000/text" 
    xmlns:xlink="http://www.w3.org/1999/xlink"
	
	xmlns:label="internal-use-only"
>

<xsl:output method="xml" indent="yes"
    doctype-public="-//OpenOffice.org//DTD OfficeDocument 1.0//EN"
    doctype-system="office.dtd"/>

<xsl:template match="/">
<office:document-content
	xmlns:office="http://openoffice.org/2000/office"
	xmlns:style="http://openoffice.org/2000/style"
	xmlns:text="http://openoffice.org/2000/text"
	xmlns:table="http://openoffice.org/2000/table"
	xmlns:draw="http://openoffice.org/2000/drawing"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:xlink="http://www.w3.org/1999/xlink"
	xmlns:number="http://openoffice.org/2000/datastyle"
	xmlns:presentation="http://openoffice.org/2000/presentation"
	xmlns:svg="http://www.w3.org/2000/svg"
	xmlns:chart="http://openoffice.org/2000/chart"
	xmlns:dr3d="http://openoffice.org/2000/dr3d"
	xmlns:math="http://www.w3.org/1998/Math/MathML"
	xmlns:form="http://openoffice.org/2000/form"
	xmlns:script="http://openoffice.org/2000/script"
	office:class="drawing" office:version="1.0">
<office:script/>
<office:automatic-styles>
	<style:style style:name="dp1" style:family="drawing-page"/>
	
	<style:style style:name="solidLine" style:family="graphics"
	 style:parent-style-name="standard">
		<style:properties
			svg:stroke-color="#000000"
			draw:fill="none"/>
	</style:style>

	<draw:stroke-dash draw:name="3 by 3" draw:style="rect"
		draw:dots1="3" draw:dots1-length="0.203cm"
		draw:dots2="3" draw:dots2-length="0.203cm"
		draw:distance="0.203cm"/>
	
	<style:style style:name="dashLine" style:family="graphics"
	 style:parent-style-name="standard">
		<style:properties
			draw:stroke="dash" draw:stroke-dash="3 by 3"
			svg:stroke-color="#000000"
			draw:fill="none"/>
	</style:style>

	<style:style style:name="redfill" style:family="graphics"
	 style:parent-style-name="standard">
		<style:properties draw:stroke="none"
			draw:fill="solid" draw:fill-color="#ff0000"/>
	</style:style>

	<style:style style:name="bluefill" style:family="graphics"
	 style:parent-style-name="standard">
		<style:properties draw:stroke="none"
			draw:fill="solid" draw:fill-color="#0000ff"/>
	</style:style>

	<style:style style:name="greenfill" style:family="graphics"
	 style:parent-style-name="standard">
		<style:properties draw:stroke="none"
			draw:fill="solid" draw:fill-color="#008000"/>
	</style:style>
	
	<style:style style:name="textgraphics" style:family="graphics"
	  style:parent-style-name="standard">
	  	<style:properties draw:stroke="none" draw:fill="none"/>
	</style:style>

	<style:style style:name="smallLeft" style:family="paragraph">
		<style:properties
			fo:font-size="10pt"
			fo:font-family="Helvetica"/>
	</style:style>

	<style:style style:name="smallRight" style:family="paragraph">
		<style:properties
			fo:font-size="10pt"
			fo:font-family="Helvetica"
			fo:text-align="end"/>
	</style:style>
	
	<style:style style:name="smallCenter" style:family="paragraph">
		<style:properties
			fo:font-size="10pt"
			fo:font-family="Helvetica"
			fo:text-align="center"/>
	</style:style>
	
	<style:style style:name="largetext" style:family="paragraph">
		<style:properties
			fo:font-size="12pt"
			fo:font-family="Helvetica"/>
	</style:style>

</office:automatic-styles>
<office:body>
<draw:page draw:name="page1" draw:style-name="dp1"
	draw:master-page-name="Default">
	<xsl:apply-templates select="Reports"/>
</draw:page>
</office:body>
</office:document-content>  	
</xsl:template>

<xsl:template match="Reports">
	<xsl:apply-templates select="SYN/@SName"/>
	<xsl:apply-templates select="SYN/SYG/@Vis"/>
	<xsl:apply-templates select="SYN/SYG/@Wind"/>
	<xsl:apply-templates select="SYN/SYG/@T"/>
</xsl:template>

<xsl:template match="@SName">
<xsl:variable name="shortName" select="substring-after(., ', ')"/>
<xsl:variable name="nameWidth"
	select="(string-length($shortName) * 10) * 0.0353"/>
<draw:text-box
	draw:style-name="textgraphics"
	draw:layer="layout"
	svg:x="0.75cm" svg:y="2cm"
	svg:width="{$nameWidth}cm" svg:height=".5cm">
    <text:p text:style-name="largetext"><xsl:value-of
		select="$shortName"/></text:p>
   </draw:text-box>
</xsl:template>

<xsl:template match="@Vis">

<!-- calculate the number of centimeters to fill -->
<xsl:variable name="visWidth">
	<xsl:choose>
		<xsl:when test=". = 'INF'">8</xsl:when>
		<xsl:when test=". &gt; 40000">8</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select=". * 8 div 40000.00"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:variable>

<!-- draw the filled area -->
<draw:rect draw:style-name="greenfill" draw:layer="layout"
	svg:x="6cm" svg:y="2cm"
	svg:width="{$visWidth}cm" svg:height="1cm" />

<!-- draw the empty bar -->
<draw:rect draw:style-name="solidLine" draw:layer="layout"
	svg:x="6cm" svg:y="2cm"
	svg:width="8cm" svg:height="1cm" />

<!-- tick marks -->
<draw:path draw:style-name="solidLine" draw:layer="layout"
	svg:x="8cm" svg:y="3cm" svg:width="8cm" svg:height="1cm"
	svg:viewBox="0 0 8000 1000"
	svg:d="m 0 0 l 0 125 m 2000 -125 l 0 125 m 2000 -125 l 0 125"/> 

<!-- create text under the tick marks -->
<xsl:for-each select="document('')/*/label:visibility/label:text">
	<draw:text-box draw:style-name="textgraphics" draw:layer="layout"
		svg:y="3.125cm"
		svg:width="0.75cm" svg:height=".5cm">
		<xsl:attribute name="svg:x">
			<xsl:value-of select="@x + 5.625"/>
			<xsl:text>cm</xsl:text>
		</xsl:attribute>
    	<text:p text:style-name="smallCenter"><xsl:value-of
			select="."/></text:p>
	</draw:text-box>
</xsl:for-each>

<draw:text-box draw:style-name="textgraphics"
	draw:layer="layout"
	svg:x="6cm" svg:y="3.7cm" svg:height="1cm" svg:width="8cm">
	<text:p text:style-name="smallCenter">Visibility (m)</text:p>
	<text:p text:style-name="smallCenter"><xsl:value-of select="."/></text:p>
</draw:text-box>

</xsl:template>

<label:visibility>
	<label:text x="0">0</label:text>
	<label:text x="2">10</label:text>
	<label:text x="4">20</label:text>
	<label:text x="6">30</label:text>
	<label:text x="8">40+</label:text>
</label:visibility>

<xsl:template match="@Wind">
<draw:circle draw:style-name="solidLine" draw:layer="layout"
	svg:x="6cm" svg:y="5.5cm"
	svg:width="4cm" svg:height="4cm"/>

<xsl:variable name="dir" select="substring-before(., ',')"/>
<xsl:variable name="dir1">
    <xsl:choose>
    <xsl:when test="contains($dir, '-')">
        <xsl:value-of select="90 - number(substring-before($dir, '-' ))"/>
    </xsl:when>
    <xsl:otherwise>
        <xsl:value-of select="90 - number($dir)"/>
    </xsl:otherwise>
    </xsl:choose>
</xsl:variable>

<xsl:variable name="dir2">
    <xsl:choose>
    <xsl:when test="contains($dir, '-')">
        <xsl:value-of select="90 - number(substring-after($dir, '-' ))"/>
    </xsl:when>
    <xsl:otherwise>
        <xsl:value-of select="90 - number($dir)"/>
    </xsl:otherwise>
    </xsl:choose>
</xsl:variable>


<draw:path
	draw:layer="layout"
	svg:viewbox="0 0 2000 100"
	svg:d="m 0 0 2000 0"
	draw:transform="rotate({$dir1 * 3.14159 div 180})
	 translate(8cm 7.5cm)">
	<xsl:attribute name="draw:style-name">
		<xsl:choose>
			<xsl:when test="$dir1 = $dir2">
				<xsl:text>solidLine</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>dashLine</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:attribute>
</draw:path>

<xsl:if test="$dir1 != $dir2">
	<draw:path draw:style-name="dashLine"
		draw:layer="layout"
		svg:viewbox="0 0 2000 100"
		svg:d="m 0 0 2000 0"
		draw:transform="rotate({$dir2 * 3.14159 div 180})
		 translate(8cm 7.5cm)"/>
</xsl:if>

<!-- tick marks -->
<draw:path draw:style-name="solidLine" draw:layer="layout"
	svg:x="6cm" svg:y="5.5cm" svg:width="4.5cm" svg:height="4.5cm"
	svg:viewBox="0 0 4500 4500"
	svg:d="M 2000 0 v 125 M 4000 2000 h -125 M 2000 4000 v -125 M 0 2000 h 125"/> 

<!-- create text near the tick marks -->
<xsl:for-each select="document('')/*/label:compass/label:text">
	<draw:text-box draw:style-name="textgraphics" draw:layer="layout"
		svg:width="0.75cm" svg:height=".5cm">
		<xsl:attribute name="svg:x"><xsl:value-of
			select="5.625 + @x"/>cm</xsl:attribute>
		<xsl:attribute name="svg:y"><xsl:value-of
			select="5.25 + @y"/>cm</xsl:attribute>
    	<text:p text:style-name="smallCenter"><xsl:value-of
			select="."/></text:p>
	</draw:text-box>
</xsl:for-each>

<!-- text under the wind compass -->
<draw:text-box
	draw:layer="layout"
	svg:x="6cm" svg:y="10cm" svg:height="1cm" svg:width="4cm">
	<text:p text:style-name="smallCenter">Wind Speed (m/sec)</text:p>
	<text:p text:style-name="smallCenter"><xsl:value-of select="$dir"/></text:p>
</draw:text-box>
</xsl:template>

<label:compass>
	<label:text x="2" y="-0.25">N</label:text>
	<label:text x="4.375" y="2">E</label:text>
	<label:text x="2" y="4.25">S</label:text>
	<label:text x="-0.375" y="2">W</label:text>
</label:compass>


<xsl:template match="@T">

<xsl:variable name="fillcolor">
	<xsl:choose>
		<xsl:when test=". &lt; 0">bluefill</xsl:when>
		<xsl:otherwise>redfill</xsl:otherwise>
	</xsl:choose>
</xsl:variable>

<!-- fill the straight part of the thermometer -->
<xsl:variable name="h" select="50*(. + 40)"/>
<draw:polygon draw:style-name="{$fillcolor}" draw:layer="layout"
	svg:x="2.25cm" svg:y="{7.5 - $h div 1000}cm"
	svg:width="0.5cm" svg:height="{$h div 1000}cm"
	svg:viewBox="0 0 500 {$h}"
	draw:points="0,{$h} 0,0 500,0 500,{$h}" />

<!-- fill the bulb -->
<draw:circle draw:style-name="{$fillcolor}" draw:layer="layout"
    draw:kind="cut"
    draw:start-angle="117" draw:end-angle="63"
    svg:x="2cm" svg:y="7.415cm"
    svg:width="1cm" svg:height="1cm"/>

<!-- draw straight part of thermometer -->
<draw:polyline draw:style-name="solidLine" draw:layer="layout"
	svg:x="2.25cm" svg:y="3cm"
	svg:width="0.6cm" svg:height="4.6cm"
	svg:viewBox="0 0 600 4600"
	draw:points="0,4500 0,0 500,0 500,4500" />

<!-- draw the bulb outline -->
<draw:circle draw:style-name="solidLine" draw:layer="layout"
    draw:kind="arc"
    draw:start-angle="117" draw:end-angle="63"
    svg:x="2cm" svg:y="7.415cm"
    svg:width="1cm" svg:height="1cm"/>


<xsl:for-each select="document('')/*/label:thermometer/label:text">
	<draw:text-box draw:style-name="textgraphics" draw:layer="layout"
		svg:width="0.75cm" svg:height=".5cm">
		<xsl:attribute name="svg:x"><xsl:value-of
			select="1.4 + @x"/>cm</xsl:attribute>
		<xsl:attribute name="svg:y"><xsl:value-of
			select="3 + @y"/>cm</xsl:attribute>
    	<text:p text:style-name="{@tstyle}"><xsl:value-of
			select="."/></text:p>
	</draw:text-box>
</xsl:for-each>

<draw:text-box draw:style-name="textgraphics"
	draw:layer="layout"
	svg:x="1cm" svg:y="8.5cm" svg:height="1cm" svg:width="3cm">
	<text:p text:style-name="smallCenter">Temp.</text:p>
	<text:p text:style-name="smallCenter"><xsl:value-of select="."/>
	<xsl:text> / </xsl:text>
	<xsl:value-of select="round((.  div 5) * 9 + 32)"/></text:p>
</draw:text-box>
</xsl:template>

<label:thermometer>
	<label:text x="0" y="-0.175" tstyle="smallRight">50</label:text>
	<label:text x="0" y="2.075" tstyle="smallRight">0</label:text>
	<label:text x="0" y="4.1" tstyle="smallRight">-40</label:text>
	<label:text x="-0.5" y="4.5" tstyle="smallRight">C</label:text>
	<label:text x="1.5" y="-0.175" tstyle="smallLeft">120</label:text>
	<label:text x="1.5" y="2.075" tstyle="smallLeft">32</label:text>
	<label:text x="1.5" y="4.1" tstyle="smallLeft">-40</label:text>
	<label:text x="2" y="4.5" tstyle="smallLeft">F</label:text>
</label:thermometer>

</xsl:stylesheet>
