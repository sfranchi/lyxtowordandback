#LyX 2.0 created this file. For more info see http://www.lyx.org/
\lyxformat 413
\begin_document
\begin_header
\textclass memoir
\begin_preamble
\usepackage[natbib=true, style=authoryear,backend=biber]{biblatex}
\addbibresource{/home/stefano/Documents/lyxtowordandback/tests/test.bib}
% customize biblatex output
\DeclareListFormat{language}{}
\DeclareFieldFormat{isbn}{}
\DeclareFieldFormat{issn}{}
\DeclareFieldFormat{series}{}
\DeclareFieldFormat{doi}{}
\DeclareFieldFormat{url}{}
\DeclareFieldFormat{eprint}{}
\DeclareFieldFormat{note}{}

\usepackage{lipsum}
\usepackage{tgtermes}
\usepackage{tgheros}
\usepackage{tgcursor}
\nouppercaseheads
%\usepackage{libertine}
\end_preamble
\options article, 11pt
\use_default_options true
\begin_modules
biblatex
logicalmkup
\end_modules
\maintain_unincluded_children false
\language english
\language_package babel
\inputencoding utf8-plain
\fontencoding global
\font_roman default
\font_sans default
\font_typewriter default
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100

\graphics default
\default_output_format pdf4
\output_sync 0
\bibtex_command biber
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry false
\use_amsmath 1
\use_esint 1
\use_mhchem 1
\use_mathdots 1
\cite_engine natbib_authoryear
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\use_refstyle 0
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 0
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle headings
\tracking_changes false
\output_changes true
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Chapter
\begin_inset CommandInset label
LatexCommand label
name "chap:Cross-references"

\end_inset

Cross-references
\end_layout

\begin_layout Standard
Here we are testing some cross-references.
 The labels to which we refer are both in this chapter and in most other
 chapters, in order to test with cross-references to different kinds of
 objects.
 
\end_layout

\begin_layout Section
Cross-references to headers
\end_layout

\begin_layout Standard
First we test cross-references for headings, beginning with the chapter
 on cross-references itself in bare form (see 
\begin_inset CommandInset ref
LatexCommand formatted
reference "chap:Cross-references"

\end_inset

), in parenthesis (see 
\begin_inset CommandInset ref
LatexCommand eqref
reference "chap:Cross-references"

\end_inset

) and with a page reference (see 
\begin_inset CommandInset ref
LatexCommand vref
reference "chap:Cross-references"

\end_inset

) (this style of reference does not seem to work with headers, though).
 Notice that in this case we would expect the cross-reference to have, optimally
 speaking, an updated, real reference to the page number in the destination
 file.
\end_layout

\begin_layout Section
To lists
\end_layout

\begin_layout Standard
Cross-references to itemized and numbered lists, both flat and nested.
 Notice that LaTeX deals with cross-references to itemized lists (which
 have no number useful for the cross-reference) by using the number of the
 closest header containing the list (chapter, section, etc).
 Thus, all references to itemized lists below should have a reference to
 a section.
 On the contrary, the reference for a numbered list is always to the item's
 number.
\end_layout

\begin_layout Standard
See for instance the item in itemized, flat list 
\begin_inset CommandInset ref
LatexCommand ref
reference "item-in-itemized-flat-list"

\end_inset


\end_layout

\begin_layout Standard
See for instance the item in itemized, flat list on the relative page: 
\begin_inset CommandInset ref
LatexCommand vref
reference "item-in-itemized-flat-list"

\end_inset


\end_layout

\begin_layout Standard
See for instance the item in itemized, nested list: 
\begin_inset CommandInset ref
LatexCommand ref
reference "item-in-itemized-nested-list"

\end_inset


\end_layout

\begin_layout Standard
See for instance the item in itemized, nested list on the relative page:
 
\begin_inset CommandInset ref
LatexCommand vref
reference "item-in-itemized-nested-list"

\end_inset


\end_layout

\begin_layout Standard
See for instance the item in numbered, flat list: 
\begin_inset CommandInset ref
LatexCommand ref
reference "enu:item-in-flat-numbered-list"

\end_inset


\end_layout

\begin_layout Standard
See for instance the item in numbered, flat list on the relative page: 
\begin_inset CommandInset ref
LatexCommand vref
reference "enu:item-in-flat-numbered-list"

\end_inset


\end_layout

\begin_layout Standard
See for instance the item in numbered, nested list: 
\begin_inset CommandInset ref
LatexCommand ref
reference "enu:item-in-nested-numbered-list"

\end_inset


\end_layout

\begin_layout Standard
See for instance the item in numbered, nested list on the relative page:
 
\begin_inset CommandInset ref
LatexCommand vref
reference "enu:item-in-nested-numbered-list"

\end_inset


\end_layout

\begin_layout Section
To equations
\end_layout

\begin_layout Standard
Here we cross-refs some of the equations in chapter 
\begin_inset CommandInset ref
LatexCommand vref
reference "chap:Mathematical-expressions"

\end_inset

.
 Here is a reference to the simple equation 
\begin_inset CommandInset ref
LatexCommand ref
reference "eqn1"

\end_inset

 that appears on page 
\begin_inset CommandInset ref
LatexCommand pageref
reference "eqn1"

\end_inset

.
 Next comes a reference to an equation environment 
\end_layout

\begin_layout Section
To figures
\end_layout

\begin_layout Standard
Here is a cross-reference to a figure in a float 
\begin_inset CommandInset ref
LatexCommand eqref
reference "fig:A-float-with-subfigure"

\end_inset

 that appears on page 
\begin_inset CommandInset ref
LatexCommand pageref
reference "fig:A-float-with-subfigure"

\end_inset

 and reference to one of its subfigures 
\begin_inset CommandInset ref
LatexCommand eqref
reference "fig:A-diagram-as-subfigure"

\end_inset

.
\end_layout

\begin_layout Section
To tables
\end_layout

\begin_layout Standard
A reference to the table in a float 
\begin_inset CommandInset ref
LatexCommand eqref
reference "tab:A-table-in-float"

\end_inset

 that appears on page 
\begin_inset CommandInset ref
LatexCommand pageref
reference "tab:A-table-in-float"

\end_inset

.
\end_layout

\begin_layout Section
To footnotes
\end_layout

\begin_layout Standard
See for instance the text in the long footnote 
\begin_inset CommandInset ref
LatexCommand eqref
reference "fn:Long-footnote"

\end_inset

 and also the the text in footnote 
\begin_inset CommandInset ref
LatexCommand eqref
reference "fn:Short-footnote"

\end_inset

 which can be found respectively 
\begin_inset CommandInset ref
LatexCommand vpageref
reference "fn:Long-footnote"

\end_inset

 and 
\begin_inset CommandInset ref
LatexCommand vpageref
reference "fn:Short-footnote"

\end_inset


\end_layout

\begin_layout Section
To footnote from footnote
\end_layout

\begin_layout Standard
This happens often in scholarly work in the Humanities, especially in Literary
 studies (especially Classics) and in Continental Europe styled philosophy
 and social sciences.
 Here is a footnote with the the first reference (no page number)
\begin_inset Foot
status open

\begin_layout Plain Layout
See for instance the short footnote 
\begin_inset CommandInset ref
LatexCommand ref
reference "fn:Short-footnote"

\end_inset


\end_layout

\end_inset

 and another footnote with the second kind (with page number).
\begin_inset Foot
status open

\begin_layout Plain Layout
See for instance the long footnote 
\begin_inset CommandInset ref
LatexCommand vref
reference "fn:Long-footnote"

\end_inset


\end_layout

\end_inset


\end_layout

\end_body
\end_document
