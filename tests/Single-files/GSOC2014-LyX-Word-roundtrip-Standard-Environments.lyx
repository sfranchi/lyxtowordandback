#LyX 2.0 created this file. For more info see http://www.lyx.org/
\lyxformat 413
\begin_document
\begin_header
\textclass memoir
\begin_preamble
\usepackage[natbib=true, style=authoryear,backend=biber]{biblatex}
\addbibresource{/home/stefano/Documents/lyxtowordandback/tests/test.bib}
% customize biblatex output
\DeclareListFormat{language}{}
\DeclareFieldFormat{isbn}{}
\DeclareFieldFormat{issn}{}
\DeclareFieldFormat{series}{}
\DeclareFieldFormat{doi}{}
\DeclareFieldFormat{url}{}
\DeclareFieldFormat{eprint}{}
\DeclareFieldFormat{note}{}

\usepackage{lipsum}
\usepackage{tgtermes}
\usepackage{tgheros}
\usepackage{tgcursor}
\nouppercaseheads
%\usepackage{libertine}
\end_preamble
\options article, 11pt
\use_default_options true
\begin_modules
biblatex
logicalmkup
\end_modules
\maintain_unincluded_children false
\language english
\language_package babel
\inputencoding utf8-plain
\fontencoding global
\font_roman default
\font_sans default
\font_typewriter default
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100

\graphics default
\default_output_format pdf4
\output_sync 0
\bibtex_command biber
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry false
\use_amsmath 1
\use_esint 1
\use_mhchem 1
\use_mathdots 1
\cite_engine natbib_authoryear
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\use_refstyle 0
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 0
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle headings
\tracking_changes false
\output_changes true
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Chapter
Standard environments
\end_layout

\begin_layout Standard
This document has sample of texts in the standard environments offered by
 LaTeX.
 Notice that the 
\begin_inset Quotes eld
\end_inset

Frontmatter
\begin_inset Quotes erd
\end_inset

 environments (Title, author, abstract, epigraph) are tested in the main
 document, while the list environments (Itemize, Enumerate) are tested in
 the 
\begin_inset Quotes eld
\end_inset

List
\begin_inset Quotes erd
\end_inset

 document.
 This paragraph is 
\begin_inset Quotes eld
\end_inset

Standard,
\begin_inset Quotes erd
\end_inset

 immediately followed by a quote;
\end_layout

\begin_layout Quote
This is a quote, spanning a few lines.
 This is a quote, spanning a few lines.
 This is a quote, spanning a few lines.
 This is a quote, spanning a few lines.
 This is a quote, spanning a few lines.
 This is a quote, spanning a few lines.
 This is a quote, spanning a few lines.
 This is a quote, spanning a few lines.
 This is a quote, spanning a few lines.
 This is a quote, spanning a few lines.
 This is a quote, spanning a few lines.
 This is a quote, spanning a few lines.
 
\end_layout

\begin_layout Standard
We are now back in the standard environment immediately after the quote.
 This paragraph should not have first line indentation and it should be
 using the same paragraph style as the first paragraph in the document.
\end_layout

\begin_layout Standard
This is a paragraph in standard style again, but since it occurs right after
 a standard environment, it should have an indented first line.
 It should also be assigned to a different paragraph style.
 We now have a 
\emph on
quotation
\emph default
 environment spanning two paragraphs:
\end_layout

\begin_layout Quotation
This is a quote, spanning a few lines.
 This is a quote, spanning a few lines.
 This is a quote, spanning a few lines.
 This is a quote, spanning a few lines.
 This is a quote, spanning a few lines.
 This is a quote, spanning a few lines.
 This is a quote, spanning a few lines.
 This is a quote, spanning a few lines.
 This is a quote, spanning a few lines.
 This is a quote, spanning a few lines.
 This is a quote, spanning a few lines.
 This is a quote, spanning a few lines.
 
\end_layout

\begin_layout Quotation
This is a quote, spanning a few lines.
 This is a quote, spanning a few lines.
 This is a quote, spanning a few lines.
 This is a quote, spanning a few lines.
 This is a quote, spanning a few lines.
 This is a quote, spanning a few lines.
 This is a quote, spanning a few lines.
 This is a quote, spanning a few lines.
 This is a quote, spanning a few lines.
 This is a quote, spanning a few lines.
 This is a quote, spanning a few lines.
 This is a quote, spanning a few lines.
 
\end_layout

\begin_layout Standard
We are now back in the standard environment immediately after the quote.
 This paragraph should not have first line indentation and it should be
 using the same paragraph style as the first paragraph in the document.
 We are now testing a few lines of poetry using the verse environments (a
 sonnet formatted as 4-4-3-3, with newlines separating the lines and separate
 environments for the stanzas):
\end_layout

\begin_layout Verse
From fairest creatures we desire increase,
\begin_inset Newline newline
\end_inset

That thereby beauty's rose might never die,
\begin_inset Newline newline
\end_inset

But as the riper should by time decease,
\begin_inset Newline newline
\end_inset

His tender heir might bear his memory:
\end_layout

\begin_layout Verse
But thou contracted to thine own bright eyes,
\begin_inset Newline newline
\end_inset

Feed'st thy light's flame with self-substantial fuel,
\begin_inset Newline newline
\end_inset

Making a famine where abundance lies,
\begin_inset Newline newline
\end_inset

Thy self thy foe, to thy sweet self too cruel:
\end_layout

\begin_layout Verse
Thou that art now the world's fresh ornament,
\begin_inset Newline newline
\end_inset

And only herald to the gaudy spring,
\begin_inset Newline newline
\end_inset

Within thine own bud buriest thy content,
\end_layout

\begin_layout Verse
And, tender churl, mak'st waste in niggarding:
\begin_inset Newline newline
\end_inset

Pity the world, or else this glutton be,
\begin_inset Newline newline
\end_inset

To eat the world's due, by the grave and thee.
\end_layout

\begin_layout Standard
We are now back in the standard environment immediately after the quote.
 This paragraph should not have first line indentation and it should be
 using the same paragraph style as the first paragraph in the document.
 
\end_layout

\begin_layout Standard
Our next standard environment is the 
\begin_inset Quotes eld
\end_inset

Description
\begin_inset Quotes erd
\end_inset

.
 We will have two consecutive descriptions, to check for formatting and
 alignment.
\end_layout

\begin_layout Description
Apple The apple is the pomaceous fruit of the apple tree, species 
\emph on
Malus domestica
\emph default
 in the rose family (Rosaceae).
 It is one of the most widely cultivated tree fruits, and the most widely
 known of the many members of genus 
\emph on
Malus
\emph default
 that are used by humans.
 Apples grow on small, deciduous trees.
 The tree originated in Central Asia, where its wild ancestor, 
\emph on
Malus sieversii
\emph default
, is still found today.
 Apples have been grown for thousands of years in Asia and Europe, and were
 brought to North America by European colonists.
 
\end_layout

\begin_layout Description
Orange The orange (specifically, the sweet orange) is the fruit of the citrus
 species
\emph on
 Citrus x sinensis
\emph default
 in the family Rutaceae.
 The fruit of the 
\emph on
Citrus sinensis
\emph default
 is considered a sweet orange, whereas the fruit of the 
\emph on
Citrus aurantium
\emph default
 is considered a bitter orange.
 The orange is a hybrid, possibly between pomelo (
\emph on
Citrus maxima
\emph default
) and mandarin (
\emph on
Citrus reticulata
\emph default
), which has been cultivated since ancient times
\end_layout

\begin_layout Standard
We are now back in the standard environment immediately after the quote.
 This paragraph should not have first line indentation and it should be
 using the same paragraph style as the first paragraph in the document.
 
\end_layout

\begin_layout Section
Non-standard yet common environments we would like to support
\end_layout

\begin_layout Standard
The next environment is LyX-Code, which is non-standard, but commonly used:
\end_layout

\begin_layout LyX-Code
#include<stdio.h>
\end_layout

\begin_layout LyX-Code
#include<time.h>
\end_layout

\begin_layout LyX-Code
#include<windows.h>
\end_layout

\begin_layout LyX-Code
#include <stdlib.h>
\end_layout

\begin_layout LyX-Code
int main()
\end_layout

\begin_layout LyX-Code
{
\end_layout

\begin_layout LyX-Code
struct tm *tmp;
\end_layout

\begin_layout LyX-Code
time_t s;
\end_layout

\begin_layout LyX-Code
for(;;)
\end_layout

\begin_layout LyX-Code
{
\end_layout

\begin_layout LyX-Code
s = time(NULL);
\end_layout

\begin_layout LyX-Code
tmp = localtime(&t);
\end_layout

\begin_layout LyX-Code
	printf("%d:%0d:%d
\backslash
n",tmp->tm_hour,tmp->tm_min,tmp->tm_sec);
\end_layout

\begin_layout LyX-Code
    
\end_layout

\begin_layout LyX-Code
	Sleep(1000);
\end_layout

\begin_layout LyX-Code
	
\end_layout

\begin_layout LyX-Code
   	system ("cls");
\end_layout

\begin_layout LyX-Code
}  
\end_layout

\begin_layout LyX-Code
	
\end_layout

\begin_layout LyX-Code
return 0;
\end_layout

\begin_layout LyX-Code
}
\end_layout

\begin_layout Standard
The we have a few environments that are memoir-specific, yet very useful
 for works i nthe Humanities and beyond.
 The first one is the 
\begin_inset Quotes eld
\end_inset

poemtitle
\begin_inset Quotes erd
\end_inset

.
 Here we have the same poem as above, preceded by its title:
\end_layout

\begin_layout Poemtitle
Sonnet 1
\end_layout

\begin_layout Verse
From fairest creatures we desire increase,
\begin_inset Newline newline
\end_inset

That thereby beauty's rose might never die,
\begin_inset Newline newline
\end_inset

But as the riper should by time decease,
\begin_inset Newline newline
\end_inset

His tender heir might bear his memory:
\end_layout

\begin_layout Verse
But thou contracted to thine own bright eyes,
\begin_inset Newline newline
\end_inset

Feed'st thy light's flame with self-substantial fuel,
\begin_inset Newline newline
\end_inset

Making a famine where abundance lies,
\begin_inset Newline newline
\end_inset

Thy self thy foe, to thy sweet self too cruel:
\end_layout

\begin_layout Verse
Thou that art now the world's fresh ornament,
\begin_inset Newline newline
\end_inset

And only herald to the gaudy spring,
\begin_inset Newline newline
\end_inset

Within thine own bud buriest thy content,
\end_layout

\begin_layout Quote
And, tender churl, mak'st waste in niggarding:
\begin_inset Newline newline
\end_inset

Pity the world, or else this glutton be,
\begin_inset Newline newline
\end_inset

To eat the world's due, by the grave and thee.
\end_layout

\begin_layout Standard
Here is an example of a 
\begin_inset Quotes eld
\end_inset

chapterprecis
\begin_inset Quotes erd
\end_inset

 which typically occurs right after the chapter title.
 Notice that the text will also be inserted in the TOC.
 See memoir manual (p.407, TL 2013 edition for how to remove it.
 It basically involves adding a line to the definition of 
\backslash
chapterToc):
\end_layout

\begin_layout Chapterprecis
This is a chapter precis.
 
\end_layout

\begin_layout Standard
Finally, here is an example of a subparagraph style:
\end_layout

\begin_layout Subparagraph*
My subparagraph.
\end_layout

\begin_layout Standard
This is the text following the subparagraph heading.
 It should flow continuously under the title of the subparagraph.
\end_layout

\end_body
\end_document
